module Day18 exposing (Branch(..), ExplosionCursor, ExplosionDescriptor, Number(..), ReductionProgress(..), add, addToLeft, addToLeftmostLeaf, addToRight, addToRightmostLeaf, eliminateExploded, explodeIfNeeded, findExplosionCandidate, findExplosionCandidateFor, inOrderTraversal, input, leafParser, listToNumber, magnitude, nodeParser, parser, performAllReductionSteps, reduce, sample, solutionPart1, solutionPart2, solvePart1For, splitIfNeeded, toString)

import List.Extra
import Parser exposing (..)


solutionPart1 : () -> Int
solutionPart1 _ =
    solvePart1For input


solvePart1For : List String -> Int
solvePart1For strings =
    let
        inputNumbers =
            strings |> List.map (Parser.run parser >> Result.withDefault InvalidNumber)

        finalSum =
            List.Extra.foldl1 (\b -> \a -> add a b |> reduce) inputNumbers
    in
    case finalSum of
        Just someSum ->
            magnitude someSum

        Nothing ->
            -1


solutionPart2 : () -> Int
solutionPart2 _ =
    solvePart2For input


solvePart2For : List String -> Int
solvePart2For strings =
    let
        inputNumbers =
            strings |> List.map (Parser.run parser >> Result.withDefault InvalidNumber)
    in
    allPairs inputNumbers
        |> List.map (\( a, b ) -> add a b |> reduce |> magnitude)
        |> List.maximum
        |> Maybe.withDefault -1


allPairs : List Number -> List ( Number, Number )
allPairs numbers =
    List.concatMap
        (\b ->
            List.concatMap
                (\a ->
                    if a == b then
                        []

                    else
                        List.singleton ( a, b )
                )
                numbers
        )
        numbers


type Number
    = Node Number Number
    | Leaf Int
    | InvalidNumber


magnitude : Number -> Int
magnitude number =
    case number of
        Node left right ->
            3 * magnitude left + 2 * magnitude right

        Leaf int ->
            int

        InvalidNumber ->
            -1


add : Number -> Number -> Number
add a b =
    case ( a, b ) of
        ( InvalidNumber, _ ) ->
            InvalidNumber

        ( _, InvalidNumber ) ->
            InvalidNumber

        _ ->
            Node a b


type Branch
    = Left
    | Right


type alias ExplosionCursor =
    { number : Number
    , depth : Int
    , branching : List Branch
    , additions : Maybe ( Int, Int )
    }


type alias ExplosionDescriptor =
    { branching : List Branch
    , additionLeft : Int
    , additionRight : Int
    }


findExplosionCandidateFor : Number -> Maybe ExplosionDescriptor
findExplosionCandidateFor number =
    let
        candidate =
            findExplosionCandidate <| ExplosionCursor number 0 [] Nothing
    in
    case candidate.additions of
        Just ( leftAddition, rightAddition ) ->
            Just <|
                ExplosionDescriptor
                    (List.reverse candidate.branching)
                    leftAddition
                    rightAddition

        Nothing ->
            Nothing


findExplosionCandidate : ExplosionCursor -> ExplosionCursor
findExplosionCandidate cursor =
    case cursor.number of
        Node left right ->
            case ( cursor.depth, left, right ) of
                ( 4, Leaf leftValue, Leaf rightValue ) ->
                    { cursor | depth = 4, additions = Just ( leftValue, rightValue ) }

                _ ->
                    let
                        explosionCursorLeft =
                            findExplosionCandidate
                                { cursor
                                    | number = left
                                    , depth = cursor.depth + 1
                                    , branching = Left :: cursor.branching
                                }
                    in
                    case explosionCursorLeft.additions of
                        Just _ ->
                            explosionCursorLeft

                        Nothing ->
                            findExplosionCandidate
                                { cursor
                                    | number = right
                                    , depth = cursor.depth + 1
                                    , branching = Right :: cursor.branching
                                }

        Leaf _ ->
            cursor

        InvalidNumber ->
            cursor


explodeIfNeeded : Number -> Number
explodeIfNeeded number =
    let
        explosionCandidate =
            findExplosionCandidateFor number
    in
    case explosionCandidate of
        Just explosionDescriptor ->
            number
                |> addToLeft explosionDescriptor.branching explosionDescriptor.additionLeft
                |> addToRight explosionDescriptor.branching explosionDescriptor.additionRight
                |> eliminateExploded explosionDescriptor.branching

        Nothing ->
            number


addToLeft : List Branch -> Int -> Number -> Number
addToLeft branches amountToAdd number =
    case ( number, branches ) of
        ( Node left right, Right :: rest ) ->
            if List.all (\d -> d == Left) rest then
                Node (addToRightmostLeaf amountToAdd left) right

            else
                Node left (addToLeft rest amountToAdd right)

        ( Node left right, Left :: rest ) ->
            Node (addToLeft rest amountToAdd left) right

        _ ->
            number


addToLeftmostLeaf : Int -> Number -> Number
addToLeftmostLeaf amount number =
    case number of
        Node left right ->
            Node (addToLeftmostLeaf amount left) right

        Leaf int ->
            Leaf (int + amount)

        InvalidNumber ->
            InvalidNumber


addToRightmostLeaf : Int -> Number -> Number
addToRightmostLeaf amount number =
    case number of
        Node left right ->
            Node left (addToRightmostLeaf amount right)

        Leaf int ->
            Leaf (int + amount)

        InvalidNumber ->
            InvalidNumber


addToRight : List Branch -> Int -> Number -> Number
addToRight branches amountToAdd number =
    case ( number, branches ) of
        ( Node left right, Left :: rest ) ->
            if List.all (\d -> d == Right) rest then
                Node left (addToLeftmostLeaf amountToAdd right)

            else
                Node (addToRight rest amountToAdd left) right

        ( Node left right, Right :: rest ) ->
            Node left (addToRight rest amountToAdd right)

        _ ->
            number


eliminateExploded : List Branch -> Number -> Number
eliminateExploded branches number =
    case ( number, branches ) of
        ( Node left right, Left :: rest ) ->
            Node (eliminateExploded rest left) right

        ( Node left right, Right :: rest ) ->
            Node left (eliminateExploded rest right)

        ( _, [] ) ->
            Leaf 0

        _ ->
            number


inOrderTraversal : Number -> List Int -> List Int
inOrderTraversal number currentList =
    case number of
        Node left right ->
            currentList ++ inOrderTraversal left [] ++ inOrderTraversal right []

        Leaf int ->
            [ int ]

        InvalidNumber ->
            []



-- TODO


splitIfNeeded : Number -> Number
splitIfNeeded number =
    case number of
        InvalidNumber ->
            InvalidNumber

        Leaf int ->
            if int >= 10 then
                Node
                    (Leaf (floor (toFloat int / 2.0)))
                    (Leaf (ceiling (toFloat int / 2.0)))

            else
                number

        Node left right ->
            let
                leftSplit =
                    splitIfNeeded left
            in
            if left /= leftSplit then
                Node leftSplit right

            else
                Node left (splitIfNeeded right)


type ReductionProgress
    = MayExplodeAndSplit Number
    | MaySplit Number
    | IsReduced Number


reduce : Number -> Number
reduce number =
    performAllReductionSteps (MayExplodeAndSplit number)


performAllReductionSteps : ReductionProgress -> Number
performAllReductionSteps reductionProgress =
    case reductionProgress of
        MayExplodeAndSplit number ->
            let
                maybeExploded =
                    explodeIfNeeded number
            in
            if maybeExploded == number then
                performAllReductionSteps (MaySplit number)

            else
                performAllReductionSteps (MayExplodeAndSplit maybeExploded)

        MaySplit number ->
            let
                maybeSplit =
                    splitIfNeeded number
            in
            if maybeSplit == number then
                performAllReductionSteps (IsReduced number)

            else
                performAllReductionSteps (MayExplodeAndSplit maybeSplit)

        IsReduced number ->
            number


toString : Number -> String
toString number =
    case number of
        Node left right ->
            "[" ++ toString left ++ "," ++ toString right ++ "]"

        Leaf int ->
            String.fromInt int

        InvalidNumber ->
            "INVALID"



---------- parsing


listToNumber : List Number -> Number
listToNumber numbers =
    case numbers of
        a :: b :: [] ->
            Node a b

        _ ->
            InvalidNumber


parser =
    Parser.oneOf
        [ leafParser
        , nodeParser
        ]


nodeParser : Parser Number
nodeParser =
    Parser.succeed listToNumber
        |= Parser.sequence
            { start = "["
            , separator = ","
            , end = "]"
            , spaces = Parser.spaces
            , item = Parser.lazy (\_ -> parser)
            , trailing = Parser.Optional
            }


leafParser : Parser Number
leafParser =
    Parser.succeed (\v -> Leaf v)
        |. Parser.spaces
        |= int


sample =
    [ "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]"
    , "[[[5,[2,8]],4],[5,[[9,9],0]]]"
    , "[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]"
    , "[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]"
    , "[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]"
    , "[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]"
    , "[[[[5,4],[7,7]],8],[[8,3],8]]"
    , "[[9,3],[[9,9],[6,[4,9]]]]"
    , "[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]"
    , "[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]"
    ]


input =
    [ "[[[6,6],3],0]"
    , "[[[0,[9,9]],[[7,7],[4,6]]],[[5,6],[5,9]]]"
    , "[[[1,[6,5]],7],[[6,[3,6]],5]]"
    , "[[0,[9,[0,2]]],[[[1,9],3],[[0,1],[5,1]]]]"
    , "[[[3,[7,6]],[1,0]],[[4,[7,9]],[5,4]]]"
    , "[[0,9],[[[0,3],[2,9]],3]]"
    , "[[[[3,2],0],[7,6]],[9,[[4,6],0]]]"
    , "[[4,[[2,5],[4,0]]],[2,[8,[3,0]]]]"
    , "[[[[3,5],6],[[8,6],2]],[[6,[5,1]],[[1,0],[3,2]]]]"
    , "[[1,[[2,8],[2,9]]],[[7,[8,8]],[[2,1],[9,5]]]]"
    , "[[[4,[4,6]],[[5,7],4]],[8,0]]"
    , "[[[3,[5,9]],[1,[6,5]]],[1,9]]"
    , "[[[5,7],[4,1]],4]"
    , "[3,[[6,0],[[9,0],[8,2]]]]"
    , "[[7,[[5,4],[7,3]]],[6,[[9,0],[1,9]]]]"
    , "[[[9,3],[0,[1,4]]],[[8,0],9]]"
    , "[[7,3],[[4,2],0]]"
    , "[[[[1,6],8],1],[[[3,6],[9,9]],9]]"
    , "[[[[2,7],[1,6]],[2,6]],0]"
    , "[[[2,[0,8]],8],[[9,7],5]]"
    , "[4,[[6,3],[9,5]]]"
    , "[[[4,[0,8]],3],[[2,9],[[5,3],[2,5]]]]"
    , "[[1,0],[[6,[9,0]],[[6,3],9]]]"
    , "[[[[0,6],[1,1]],[[2,9],2]],[[2,0],3]]"
    , "[[[6,8],[[1,1],9]],[6,5]]"
    , "[1,[5,9]]"
    , "[[[[6,8],[4,6]],[[8,2],[9,2]]],[[3,[6,8]],[4,3]]]"
    , "[8,[[[2,4],[1,1]],[[7,1],[3,4]]]]"
    , "[[[[1,9],5],[[8,6],9]],[[[3,7],[9,6]],[2,0]]]"
    , "[[[7,[2,7]],[[2,9],7]],[4,[5,[9,5]]]]"
    , "[9,[[[1,9],7],[[8,7],[2,8]]]]"
    , "[9,2]"
    , "[[7,[1,[2,8]]],[9,5]]"
    , "[[[0,[5,4]],6],[8,1]]"
    , "[[[0,[9,8]],0],5]"
    , "[[9,[2,9]],[1,[8,[3,8]]]]"
    , "[3,[[5,5],[2,[2,5]]]]"
    , "[[6,7],[[[7,3],3],8]]"
    , "[[[[6,7],[2,6]],7],[0,[6,[3,5]]]]"
    , "[2,[[9,2],[[8,5],[1,2]]]]"
    , "[0,[[[0,8],[9,7]],[[5,1],7]]]"
    , "[[[2,1],6],[[9,[0,9]],[2,6]]]"
    , "[4,[9,[[5,2],[6,3]]]]"
    , "[[[9,1],4],[[6,[5,6]],[5,8]]]"
    , "[[4,[[1,5],[5,4]]],[3,[[7,2],7]]]"
    , "[[[4,5],6],[9,[9,1]]]"
    , "[3,[7,[5,2]]]"
    , "[[[0,[6,6]],[[7,8],[0,8]]],[2,[[0,5],8]]]"
    , "[[[[2,3],[0,6]],[[6,0],[9,4]]],[[[1,6],1],[[5,6],9]]]"
    , "[[[1,[2,2]],[9,[8,2]]],[[[2,9],0],[5,[2,7]]]]"
    , "[[[2,[4,9]],[2,[0,0]]],[[2,[9,7]],[[3,4],[0,7]]]]"
    , "[[1,[7,[3,5]]],[[7,[5,8]],1]]"
    , "[[[[2,3],1],[5,3]],[[0,[1,8]],[1,2]]]"
    , "[[6,9],[0,[[8,8],4]]]"
    , "[[[8,[9,1]],[[0,1],6]],[[8,8],[2,4]]]"
    , "[[0,[[5,1],[5,8]]],[[5,[5,1]],2]]"
    , "[[[8,[8,4]],8],[[2,[6,0]],[[5,8],6]]]"
    , "[5,[[1,[3,6]],[[5,8],[5,0]]]]"
    , "[8,[[5,[7,8]],[[9,9],[8,4]]]]"
    , "[2,[[[7,6],7],[[9,4],[8,9]]]]"
    , "[[5,9],[[8,[2,9]],[0,[2,8]]]]"
    , "[[[2,[7,4]],5],3]"
    , "[[[7,[1,1]],5],[[4,[7,2]],[[5,8],[2,8]]]]"
    , "[[[1,[2,8]],6],7]"
    , "[[[[2,7],[0,0]],[[5,3],5]],[[[1,1],[4,6]],7]]"
    , "[[[4,6],[1,[5,0]]],8]"
    , "[[[8,[5,3]],2],[[[0,2],[3,6]],[[7,9],[8,0]]]]"
    , "[[[2,[2,0]],5],[[[5,8],[0,1]],[8,[8,5]]]]"
    , "[[[9,[0,7]],[[5,0],[9,6]]],0]"
    , "[[[[1,6],4],[1,4]],[[[5,8],4],[[9,9],[9,7]]]]"
    , "[1,1]"
    , "[[4,4],[[6,3],9]]"
    , "[[[[4,0],[6,8]],[[6,0],0]],[8,[[7,9],7]]]"
    , "[[[[1,1],6],[[4,1],[6,4]]],4]"
    , "[[[[9,5],[5,8]],2],[[3,[8,8]],[[7,0],8]]]"
    , "[[[[9,3],4],[3,[9,2]]],[[2,7],[7,[2,3]]]]"
    , "[[[[6,8],[7,4]],[[1,6],1]],[3,4]]"
    , "[[1,[2,[8,6]]],5]"
    , "[[[[7,5],[2,5]],[[5,3],[0,3]]],[4,9]]"
    , "[[8,7],[[2,[1,6]],[[4,8],1]]]"
    , "[[9,[[0,7],[7,2]]],4]"
    , "[[4,[8,[9,6]]],[[[8,1],[3,5]],8]]"
    , "[[[[9,1],[2,2]],[[7,9],0]],[[8,[0,6]],[0,[7,3]]]]"
    , "[[[2,[8,7]],[[9,0],1]],[8,[9,0]]]"
    , "[1,[3,2]]"
    , "[[[[6,3],7],[[5,3],[3,1]]],[4,[[9,3],5]]]"
    , "[3,[1,9]]"
    , "[3,[[[6,4],[0,2]],[[3,8],[5,3]]]]"
    , "[[[[3,4],3],[[4,6],4]],[[[5,7],7],3]]"
    , "[[[5,1],[[1,0],4]],[[2,3],[0,2]]]"
    , "[[[[2,4],[8,0]],5],[[1,2],[6,[2,3]]]]"
    , "[[[2,9],4],4]"
    , "[[8,[[6,2],[1,3]]],[8,[[6,3],[4,9]]]]"
    , "[[[[2,7],9],[[5,6],[3,4]]],[5,9]]"
    , "[[[[5,5],9],1],2]"
    , "[[[[9,7],[6,9]],[[6,8],[3,9]]],[6,[3,8]]]"
    , "[[9,3],[[6,0],5]]"
    , "[[[[5,1],4],[[2,8],7]],[[[9,8],6],[[1,5],[4,0]]]]"
    , "[[[4,5],3],[[3,[5,9]],[7,[9,2]]]]"
    , "[[[[1,7],5],[0,[2,2]]],[[3,6],[9,6]]]"
    ]
