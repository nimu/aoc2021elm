module Day06 exposing (Population, applyDayAgeToPopulation, emptyPopulation, input, inputPopulation, solutionPart1, solutionPart2, totalPopulation)


type alias Population =
    { zeroDays : Int
    , oneDay : Int
    , twoDays : Int
    , threeDays : Int
    , fourDays : Int
    , fiveDays : Int
    , sixDays : Int
    , sevenDays : Int
    , eightDays : Int
    }


emptyPopulation : Population
emptyPopulation =
    Population 0 0 0 0 0 0 0 0 0


applyDayAgeToPopulation : Population -> Population
applyDayAgeToPopulation population =
    { emptyPopulation
        | zeroDays = population.oneDay
        , oneDay = population.twoDays
        , twoDays = population.threeDays
        , threeDays = population.fourDays
        , fourDays = population.fiveDays
        , fiveDays = population.sixDays
        , sixDays = population.sevenDays + population.zeroDays
        , sevenDays = population.eightDays
        , eightDays = population.zeroDays
    }


totalPopulation : Population -> Int
totalPopulation { zeroDays, oneDay, twoDays, threeDays, fourDays, fiveDays, sixDays, sevenDays, eightDays } =
    zeroDays
        + oneDay
        + twoDays
        + threeDays
        + fourDays
        + fiveDays
        + sixDays
        + sevenDays
        + eightDays


solutionPart1 : () -> Int
solutionPart1 _ =
    List.foldl (\_ -> applyDayAgeToPopulation) inputPopulation (List.range 1 80)
        |> totalPopulation



--------------------------- part 2


solutionPart2 : () -> Int
solutionPart2 _ =
    List.foldl (\_ -> applyDayAgeToPopulation) inputPopulation (List.range 1 256)
        |> totalPopulation


inputPopulation : Population
inputPopulation =
    let
        addToPopulation : Int -> Population -> Population
        addToPopulation value population =
            case value of
                0 ->
                    { population | zeroDays = population.zeroDays + 1 }

                1 ->
                    { population | oneDay = population.oneDay + 1 }

                2 ->
                    { population | twoDays = population.twoDays + 1 }

                3 ->
                    { population | threeDays = population.threeDays + 1 }

                4 ->
                    { population | fourDays = population.fourDays + 1 }

                5 ->
                    { population | fiveDays = population.fiveDays + 1 }

                6 ->
                    { population | sixDays = population.sixDays + 1 }

                7 ->
                    { population | sevenDays = population.sevenDays + 1 }

                _ ->
                    { population | eightDays = population.eightDays + 1 }
    in
    List.foldl addToPopulation emptyPopulation input


input =
    [ 1, 2, 4, 5, 5, 5, 2, 1, 3, 1, 4, 3, 2, 1, 5, 5, 1, 2, 3, 4, 4, 1, 2, 3, 2, 1, 4, 4, 1, 5, 5, 1, 3, 4, 4, 4, 1, 2, 2, 5, 1, 5, 5, 3, 2, 3, 1, 1, 3, 5, 1, 1, 2, 4, 2, 3, 1, 1, 2, 1, 3, 1, 2, 1, 1, 2, 1, 2, 2, 1, 1, 1, 1, 5, 4, 5, 2, 1, 3, 2, 4, 1, 1, 3, 4, 1, 4, 1, 5, 1, 4, 1, 5, 3, 2, 3, 2, 2, 4, 4, 3, 3, 4, 3, 4, 4, 3, 4, 5, 1, 2, 5, 2, 1, 5, 5, 1, 3, 4, 2, 2, 4, 2, 2, 1, 3, 2, 5, 5, 1, 3, 3, 4, 3, 5, 3, 5, 5, 4, 5, 1, 1, 4, 1, 4, 5, 1, 1, 1, 4, 1, 1, 4, 2, 1, 4, 1, 3, 4, 4, 3, 1, 2, 2, 4, 3, 3, 2, 2, 2, 3, 5, 5, 2, 3, 1, 5, 1, 1, 1, 1, 3, 1, 4, 1, 4, 1, 2, 5, 3, 2, 4, 4, 1, 3, 1, 1, 1, 3, 4, 4, 1, 1, 2, 1, 4, 3, 4, 2, 2, 3, 2, 4, 3, 1, 5, 1, 3, 1, 4, 5, 5, 3, 5, 1, 3, 5, 5, 4, 2, 3, 2, 4, 1, 3, 2, 2, 2, 1, 3, 4, 2, 5, 2, 5, 3, 5, 5, 1, 1, 1, 2, 2, 3, 1, 4, 4, 4, 5, 4, 5, 5, 1, 4, 5, 5, 4, 1, 1, 5, 3, 3, 1, 4, 1, 3, 1, 1, 4, 1, 5, 2, 3, 2, 3, 1, 2, 2, 2, 1, 1, 5, 1, 4, 5, 2, 4, 2, 2, 3 ]
