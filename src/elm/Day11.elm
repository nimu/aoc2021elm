module Day11 exposing
    ( Cursor
    , input
    , sample
    , solutionPart1
    , solutionPart2
    )

import Matrix exposing (Matrix)


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        initialCursor =
            Cursor (inputToBoard input) 0 0
    in
    List.foldl (\_ -> performStep) initialCursor (List.range 1 100)
        |> .flashCount


solutionPart2 : () -> Int
solutionPart2 _ =
    let
        initialCursor =
            Cursor (inputToBoard input) 0 0
    in
    findFirstStepWithHundredFlashes initialCursor


findFirstStepWithHundredFlashes : Cursor -> Int
findFirstStepWithHundredFlashes cursor =
    let
        nextCursor =
            performStep cursor
    in
    if nextCursor.flashCount - cursor.flashCount == 100 then
        nextCursor.stepNr

    else
        findFirstStepWithHundredFlashes nextCursor


type alias Board =
    Matrix Octopus


incrementBoard : Board -> Board
incrementBoard board =
    Matrix.map
        (\octopus ->
            case octopus of
                Loading i ->
                    Loading (i + 1)

                Flashed ->
                    Flashed
        )
        board


type alias Cursor =
    { board : Board
    , flashCount : Int
    , stepNr : Int
    }


increaseStepNr : Cursor -> Cursor
increaseStepNr cursor =
    { cursor | stepNr = cursor.stepNr + 1 }


performStep : Cursor -> Cursor
performStep cursor =
    { cursor | board = Debug.log "new board " <| incrementBoard cursor.board }
        |> performAllFlashes
        |> resetFlashedOctopuses
        |> increaseStepNr


incrementAtPosition : ( Int, Int ) -> Board -> Board
incrementAtPosition ( x, y ) board =
    let
        currentValue =
            Matrix.get ( x, y ) board
    in
    case currentValue of
        Just (Loading someValue) ->
            Matrix.set ( x, y ) (Loading (someValue + 1)) board

        _ ->
            board


incrementNeighborsOf : ( Int, Int ) -> Board -> Board
incrementNeighborsOf ( x, y ) board =
    List.foldl incrementAtPosition board (neighborsOf ( x, y ))


performSingleFlashAt : ( Int, Int ) -> Cursor -> Cursor
performSingleFlashAt ( x, y ) cursor =
    let
        newBoard =
            cursor.board
                |> Matrix.set ( x, y ) Flashed
                |> incrementNeighborsOf ( x, y )
    in
    { cursor | board = newBoard, flashCount = cursor.flashCount + 1 }


isAboutToFlash : Octopus -> Bool
isAboutToFlash octopus =
    case octopus of
        Loading value ->
            value > 9

        _ ->
            False


performAllFlashes : Cursor -> Cursor
performAllFlashes cursor =
    let
        octopusAboutToFlash =
            Matrix.toIndexedList cursor.board
                |> List.filter (Tuple.second >> isAboutToFlash)
                |> List.head
                |> Maybe.map Tuple.first
    in
    case octopusAboutToFlash of
        Just ( x, y ) ->
            performSingleFlashAt ( x, y ) cursor
                |> performAllFlashes

        Nothing ->
            cursor


resetFlashedOctopuses : Cursor -> Cursor
resetFlashedOctopuses cursor =
    let
        newBoard =
            Matrix.map
                (\octopus ->
                    case octopus of
                        Flashed ->
                            Loading 0

                        loading ->
                            loading
                )
                cursor.board
    in
    { cursor | board = newBoard }


inputToBoard : List String -> Board
inputToBoard rows =
    rows
        |> List.map String.toList
        |> List.map (List.map charToOctopus)
        |> Matrix.fromList


neighborsOf : ( Int, Int ) -> List ( Int, Int )
neighborsOf ( x, y ) =
    [ ( x - 1, y - 1 )
    , ( x - 1, y )
    , ( x - 1, y + 1 )
    , ( x, y - 1 )
    , ( x, y + 1 )
    , ( x + 1, y - 1 )
    , ( x + 1, y )
    , ( x + 1, y + 1 )
    ]


type Octopus
    = Loading Int
    | Flashed


charToOctopus : Char -> Octopus
charToOctopus char =
    case char of
        '0' ->
            Loading 0

        '1' ->
            Loading 1

        '2' ->
            Loading 2

        '3' ->
            Loading 3

        '4' ->
            Loading 4

        '5' ->
            Loading 5

        '6' ->
            Loading 6

        '7' ->
            Loading 7

        '8' ->
            Loading 8

        '9' ->
            Loading 9

        _ ->
            Flashed



------------------ part 2


input =
    [ "4658137637"
    , "3277874355"
    , "4525611183"
    , "3128125888"
    , "8734832838"
    , "4175463257"
    , "8321423552"
    , "4832145253"
    , "8286834851"
    , "4885323138"
    ]


sample =
    [ "5483143223"
    , "2745854711"
    , "5264556173"
    , "6141336146"
    , "6357385478"
    , "4167524645"
    , "2176841721"
    , "6882881134"
    , "4846848554"
    , "5283751526"
    ]
