module Day14 exposing
    ( solutionPart1
    , solutionPart2
    )

import Dict exposing (Dict)


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        initialCursor =
            Cursor Beginning input.polymerTemplate
    in
    List.range 1 10
        |> List.foldl (\_ -> performStep input.rules) initialCursor
        |> Debug.log "resulting cursor"
        |> .polymerOutput
        |> evaluateSolution


solutionPart2 : () -> Int
solutionPart2 _ =
    -1


evaluateSolution : String -> Int
evaluateSolution polymer =
    let
        frequenciesByChar =
            polymer
                |> String.toList
                |> List.foldl recordElement Dict.empty

        sortedFrequencies =
            frequenciesByChar
                |> Dict.values
                |> List.sort

        mostCommonElementFrequency =
            sortedFrequencies
                |> List.reverse
                |> List.head
                |> Maybe.withDefault -1

        leastCommonElementFrequency =
            sortedFrequencies |> List.head |> Maybe.withDefault -1
    in
    mostCommonElementFrequency - leastCommonElementFrequency


recordElement : Char -> Dict Char Int -> Dict Char Int
recordElement element frequencies =
    let
        currentCount =
            Dict.get element frequencies
    in
    case currentCount of
        Just count ->
            Dict.insert element (count + 1) frequencies

        Nothing ->
            Dict.insert element 1 frequencies


type SlidingElementWindow
    = Beginning
    | SingleElement Char
    | IntermediateElements Char Char
    | LastElementPair Char Char


type alias Cursor =
    { slidingElementWindow : SlidingElementWindow
    , polymerOutput : String
    }


iterate : Rules -> Maybe Char -> Cursor -> Cursor
iterate rules maybeElement cursor =
    let
        newSlidingElementWindow =
            slide maybeElement cursor.slidingElementWindow

        outputElementPair =
            outputPairOfElements rules newSlidingElementWindow
    in
    { cursor
        | slidingElementWindow = newSlidingElementWindow
        , polymerOutput = cursor.polymerOutput ++ outputElementPair
    }


performStep : Rules -> Cursor -> Cursor
performStep rules cursor =
    cursor.polymerOutput
        |> String.toList
        |> List.map Just
        |> (\list -> list ++ [ Nothing ])
        |> List.foldl (iterate rules) (Cursor Beginning "")


outputPairOfElements : Rules -> SlidingElementWindow -> String
outputPairOfElements rules slidingElementWindow =
    case slidingElementWindow of
        Beginning ->
            ""

        SingleElement _ ->
            ""

        IntermediateElements firstChar secondChar ->
            Dict.get (String.fromList [ firstChar, secondChar ]) rules
                |> Maybe.map (\insertedElement -> String.fromList [ firstChar, insertedElement ])
                |> Maybe.withDefault ""

        LastElementPair _ lastChar ->
            String.fromList [ lastChar ]


slide : Maybe Char -> SlidingElementWindow -> SlidingElementWindow
slide maybeNextElement current =
    case ( maybeNextElement, current ) of
        ( Just nextElement, Beginning ) ->
            SingleElement nextElement

        ( Just nextElement, SingleElement firstElement ) ->
            IntermediateElements firstElement nextElement

        ( Just nextElement, IntermediateElements _ secondElement ) ->
            IntermediateElements secondElement nextElement

        ( Nothing, IntermediateElements firstElement secondElement ) ->
            LastElementPair firstElement secondElement

        _ ->
            -- should never happen
            Beginning


type alias Rules =
    Dict String Char


type alias Input =
    { polymerTemplate : String
    , rules : Rules
    }


input : Input
input =
    { polymerTemplate = "CNBPHFBOPCSPKOFNHVKV"
    , rules =
        Dict.fromList
            [ ( "CS", 'S' )
            , ( "FB", 'F' )
            , ( "VK", 'V' )
            , ( "HO", 'F' )
            , ( "SO", 'K' )
            , ( "FK", 'B' )
            , ( "VS", 'C' )
            , ( "PS", 'H' )
            , ( "HH", 'P' )
            , ( "KH", 'V' )
            , ( "PV", 'V' )
            , ( "CB", 'N' )
            , ( "BB", 'N' )
            , ( "HB", 'B' )
            , ( "HV", 'O' )
            , ( "NC", 'H' )
            , ( "NF", 'B' )
            , ( "HP", 'B' )
            , ( "HK", 'S' )
            , ( "SF", 'O' )
            , ( "ON", 'K' )
            , ( "VN", 'V' )
            , ( "SB", 'H' )
            , ( "SK", 'H' )
            , ( "VH", 'N' )
            , ( "KN", 'C' )
            , ( "CC", 'N' )
            , ( "BF", 'H' )
            , ( "SN", 'N' )
            , ( "KP", 'B' )
            , ( "FO", 'N' )
            , ( "KO", 'V' )
            , ( "BP", 'O' )
            , ( "OK", 'F' )
            , ( "HC", 'B' )
            , ( "NH", 'O' )
            , ( "SP", 'O' )
            , ( "OO", 'S' )
            , ( "VC", 'O' )
            , ( "PC", 'F' )
            , ( "VB", 'O' )
            , ( "FF", 'S' )
            , ( "BS", 'F' )
            , ( "KS", 'F' )
            , ( "OV", 'P' )
            , ( "NB", 'O' )
            , ( "CF", 'F' )
            , ( "SS", 'V' )
            , ( "KV", 'K' )
            , ( "FP", 'F' )
            , ( "KC", 'C' )
            , ( "PF", 'C' )
            , ( "OS", 'C' )
            , ( "PN", 'B' )
            , ( "OP", 'C' )
            , ( "FN", 'F' )
            , ( "OF", 'C' )
            , ( "NP", 'C' )
            , ( "CK", 'N' )
            , ( "BN", 'K' )
            , ( "BO", 'K' )
            , ( "OH", 'S' )
            , ( "BH", 'O' )
            , ( "SH", 'N' )
            , ( "CH", 'K' )
            , ( "PO", 'V' )
            , ( "CN", 'N' )
            , ( "BV", 'F' )
            , ( "FV", 'B' )
            , ( "VP", 'V' )
            , ( "FS", 'O' )
            , ( "NV", 'P' )
            , ( "PH", 'C' )
            , ( "HN", 'P' )
            , ( "VV", 'C' )
            , ( "NK", 'K' )
            , ( "CO", 'N' )
            , ( "NS", 'P' )
            , ( "VO", 'P' )
            , ( "CP", 'V' )
            , ( "OC", 'S' )
            , ( "PK", 'V' )
            , ( "NN", 'F' )
            , ( "SC", 'P' )
            , ( "BK", 'F' )
            , ( "BC", 'P' )
            , ( "FH", 'B' )
            , ( "OB", 'O' )
            , ( "FC", 'N' )
            , ( "PB", 'N' )
            , ( "VF", 'N' )
            , ( "PP", 'S' )
            , ( "HS", 'O' )
            , ( "HF", 'N' )
            , ( "KK", 'C' )
            , ( "KB", 'N' )
            , ( "SV", 'N' )
            , ( "KF", 'K' )
            , ( "CV", 'N' )
            , ( "NO", 'P' )
            ]
    }


sample : Input
sample =
    { polymerTemplate = "NN"
    , rules =
        Dict.fromList
            [ ( "CH", 'B' )
            , ( "HH", 'N' )
            , ( "CB", 'H' )
            , ( "NH", 'C' )
            , ( "HB", 'C' )
            , ( "HC", 'B' )
            , ( "HN", 'C' )
            , ( "NN", 'C' )
            , ( "BH", 'H' )
            , ( "NC", 'B' )
            , ( "NB", 'B' )
            , ( "BN", 'B' )
            , ( "BB", 'N' )
            , ( "BC", 'B' )
            , ( "CC", 'N' )
            , ( "CN", 'C' )
            ]
    }
