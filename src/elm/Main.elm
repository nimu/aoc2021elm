module Main exposing (Flags, Model, Msg(..), Part(..), init, main, subscriptions, update, view)

-- Imports ---------------------------------------------------------------------

import Browser
import Day01 exposing (..)
import Day02
import Day03
import Day04
import Day05
import Day06
import Day07
import Day08
import Day09
import Day10
import Day11
import Day12
import Day13
import Day14
import Day15
import Day16
import Day17
import Day18
import Day19
import Day20
import Day21
import Day22
import Day23
import Day24
import Day25
import Html.Styled as Html
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Events
import Tailwind.Utilities as Tw



-- Main ------------------------------------------------------------------------


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- Model -----------------------------------------------------------------------


type alias Flags =
    ()


type alias Model =
    { answer : Int
    }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { answer = 0
      }
    , Cmd.none
    )



-- Update ----------------------------------------------------------------------


type Part
    = PartOne
    | PartTwo


type Msg
    = Day1 Part
    | Day2 Part
    | Day3 Part
    | Day4 Part
    | Day5 Part
    | Day6 Part
    | Day7 Part
    | Day8 Part
    | Day9 Part
    | Day10 Part
    | Day11 Part
    | Day12 Part
    | Day13 Part
    | Day14 Part
    | Day15 Part
    | Day16 Part
    | Day17 Part
    | Day18 Part
    | Day19 Part
    | Day20 Part
    | Day21 Part
    | Day22 Part
    | Day23 Part
    | Day24 Part
    | Day25 Part


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Day1 PartOne ->
            ( { model | answer = Day01.solutionPart1 () }
            , Cmd.none
            )

        Day1 PartTwo ->
            ( { model | answer = Day01.solutionPart2 () }
            , Cmd.none
            )

        Day2 PartOne ->
            ( { model | answer = Day02.solutionPart1 () }
            , Cmd.none
            )

        Day2 PartTwo ->
            ( { model | answer = Day02.solutionPart2 () }
            , Cmd.none
            )

        Day3 PartOne ->
            ( { model | answer = Day03.solutionPart1 () }
            , Cmd.none
            )

        Day3 PartTwo ->
            ( { model | answer = Day03.solutionPart2 () }
            , Cmd.none
            )

        Day4 PartOne ->
            ( { model | answer = Day04.solutionPart1 () }
            , Cmd.none
            )

        Day4 PartTwo ->
            ( { model | answer = Day04.solutionPart2 () }
            , Cmd.none
            )

        Day5 PartOne ->
            ( { model | answer = Day05.solutionPart1 () }
            , Cmd.none
            )

        Day5 PartTwo ->
            ( { model | answer = Day05.solutionPart2 () }
            , Cmd.none
            )

        Day6 PartOne ->
            ( { model | answer = Day06.solutionPart1 () }
            , Cmd.none
            )

        Day6 PartTwo ->
            ( { model | answer = Day06.solutionPart2 () }
            , Cmd.none
            )

        Day7 PartOne ->
            ( { model | answer = Day07.solutionPart1 () }
            , Cmd.none
            )

        Day7 PartTwo ->
            ( { model | answer = Day07.solutionPart2 () }
            , Cmd.none
            )

        Day8 PartOne ->
            ( { model | answer = Day08.solutionPart1 () }
            , Cmd.none
            )

        Day8 PartTwo ->
            ( { model | answer = Day08.solutionPart2 () }
            , Cmd.none
            )

        Day9 PartOne ->
            ( { model | answer = Day09.solutionPart1 () }
            , Cmd.none
            )

        Day9 PartTwo ->
            ( { model | answer = Day09.solutionPart2 () }
            , Cmd.none
            )

        Day10 PartOne ->
            ( { model | answer = Day10.solutionPart1 () }
            , Cmd.none
            )

        Day10 PartTwo ->
            ( { model | answer = Day10.solutionPart2 () }
            , Cmd.none
            )

        Day11 PartOne ->
            ( { model | answer = Day11.solutionPart1 () }
            , Cmd.none
            )

        Day11 PartTwo ->
            ( { model | answer = Day11.solutionPart2 () }
            , Cmd.none
            )

        Day12 PartOne ->
            ( { model | answer = Day12.solutionPart1 () }
            , Cmd.none
            )

        Day12 PartTwo ->
            ( { model | answer = Day12.solutionPart2 () }
            , Cmd.none
            )

        Day13 PartOne ->
            ( { model | answer = Day13.solutionPart1 () }
            , Cmd.none
            )

        Day13 PartTwo ->
            ( { model | answer = Day13.solutionPart2 () }
            , Cmd.none
            )

        Day14 PartOne ->
            ( { model | answer = Day14.solutionPart1 () }
            , Cmd.none
            )

        Day14 PartTwo ->
            ( { model | answer = Day14.solutionPart2 () }
            , Cmd.none
            )

        Day15 PartOne ->
            ( { model | answer = Day15.solutionPart1 () }
            , Cmd.none
            )

        Day15 PartTwo ->
            ( { model | answer = Day15.solutionPart2 () }
            , Cmd.none
            )

        Day16 PartOne ->
            ( { model | answer = Day16.solutionPart1 () }
            , Cmd.none
            )

        Day16 PartTwo ->
            ( { model | answer = Day16.solutionPart2 () }
            , Cmd.none
            )

        Day17 PartOne ->
            ( { model | answer = Day17.solutionPart1 () }
            , Cmd.none
            )

        Day17 PartTwo ->
            ( { model | answer = Day17.solutionPart2 () }
            , Cmd.none
            )

        Day18 PartOne ->
            ( { model | answer = Day18.solutionPart1 () }
            , Cmd.none
            )

        Day18 PartTwo ->
            ( { model | answer = Day18.solutionPart2 () }
            , Cmd.none
            )

        Day19 PartOne ->
            ( { model | answer = Day19.solutionPart1 () }
            , Cmd.none
            )

        Day19 PartTwo ->
            ( { model | answer = Day19.solutionPart2 () }
            , Cmd.none
            )

        Day20 PartOne ->
            ( { model | answer = Day20.solutionPart1 () }
            , Cmd.none
            )

        Day20 PartTwo ->
            ( { model | answer = Day20.solutionPart2 () }
            , Cmd.none
            )

        Day21 PartOne ->
            ( { model | answer = Day21.solutionPart1 () }
            , Cmd.none
            )

        Day21 PartTwo ->
            ( { model | answer = Day21.solutionPart2 () }
            , Cmd.none
            )

        Day22 PartOne ->
            ( { model | answer = Day22.solutionPart1 () }
            , Cmd.none
            )

        Day22 PartTwo ->
            ( { model | answer = Day22.solutionPart2 () }
            , Cmd.none
            )

        Day23 PartOne ->
            ( { model | answer = Day23.solutionPart1 () }
            , Cmd.none
            )

        Day23 PartTwo ->
            ( { model | answer = Day23.solutionPart2 () }
            , Cmd.none
            )

        Day24 PartOne ->
            ( { model | answer = Day24.solutionPart1 () }
            , Cmd.none
            )

        Day24 PartTwo ->
            ( { model | answer = Day24.solutionPart2 () }
            , Cmd.none
            )

        Day25 PartOne ->
            ( { model | answer = Day25.solutionPart1 () }
            , Cmd.none
            )

        Day25 PartTwo ->
            ( { model | answer = Day25.solutionPart2 () }
            , Cmd.none
            )



-- View ------------------------------------------------------------------------


view model =
    Html.toUnstyled <|
        Html.div [ Attr.css [ Tw.grid, Tw.grid_cols_4, Tw.gap_4 ] ]
            [ Html.div
                [ Attr.css
                    [ Tw.col_span_4
                    , Tw.text_6xl
                    , Tw.m_6
                    , Tw.text_blue_500
                    , Tw.text_center
                    ]
                ]
                [ Html.text <| String.fromInt model.answer ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day1 PartOne) ] [ Html.text "Day 1 Part 1" ]
                , Html.button [ Events.onClick (Day1 PartTwo) ] [ Html.text "Day 1 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day2 PartOne) ] [ Html.text "Day 2 Part 1" ]
                , Html.button [ Events.onClick (Day2 PartTwo) ] [ Html.text "Day 2 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day3 PartOne) ] [ Html.text "Day 3 Part 1" ]
                , Html.button [ Events.onClick (Day3 PartTwo) ] [ Html.text "Day 3 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day4 PartOne) ] [ Html.text "Day 4 Part 1" ]
                , Html.button [ Events.onClick (Day4 PartTwo) ] [ Html.text "Day 4 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day5 PartOne) ] [ Html.text "Day 5 Part 1" ]
                , Html.button [ Events.onClick (Day5 PartTwo) ] [ Html.text "Day 5 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day6 PartOne) ] [ Html.text "Day 6 Part 1" ]
                , Html.button [ Events.onClick (Day6 PartTwo) ] [ Html.text "Day 6 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day7 PartOne) ] [ Html.text "Day 7 Part 1" ]
                , Html.button [ Events.onClick (Day7 PartTwo) ] [ Html.text "Day 7 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day8 PartOne) ] [ Html.text "Day 8 Part 1" ]
                , Html.button [ Events.onClick (Day8 PartTwo) ] [ Html.text "Day 8 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day9 PartOne) ] [ Html.text "Day 9 Part 1" ]
                , Html.button [ Events.onClick (Day9 PartTwo) ] [ Html.text "Day 9 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day10 PartOne) ] [ Html.text "Day 10 Part 1" ]
                , Html.button [ Events.onClick (Day10 PartTwo) ] [ Html.text "Day 10 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day11 PartOne) ] [ Html.text "Day 11 Part 1" ]
                , Html.button [ Events.onClick (Day11 PartTwo) ] [ Html.text "Day 11 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day12 PartOne) ] [ Html.text "Day 12 Part 1" ]
                , Html.button [ Events.onClick (Day12 PartTwo) ] [ Html.text "Day 12 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day13 PartOne) ] [ Html.text "Day 13 Part 1" ]
                , Html.button [ Events.onClick (Day13 PartTwo) ] [ Html.text "Day 13 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day14 PartOne) ] [ Html.text "Day 14 Part 1" ]
                , Html.button [ Events.onClick (Day14 PartTwo) ] [ Html.text "Day 14 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day15 PartOne) ] [ Html.text "Day 15 Part 1" ]
                , Html.button [ Events.onClick (Day15 PartTwo) ] [ Html.text "Day 15 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Attr.disabled True, Events.onClick (Day16 PartOne) ] [ Html.text "Day 16 Part 1" ]
                , Html.button [ Attr.disabled True, Events.onClick (Day16 PartTwo) ] [ Html.text "Day 16 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day17 PartOne) ] [ Html.text "Day 17 Part 1" ]
                , Html.button [ Events.onClick (Day17 PartTwo) ] [ Html.text "Day 17 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day18 PartOne) ] [ Html.text "Day 18 Part 1" ]
                , Html.button [ Events.onClick (Day18 PartTwo) ] [ Html.text "Day 18 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Attr.disabled True, Events.onClick (Day19 PartOne) ] [ Html.text "Day 19 Part 1" ]
                , Html.button [ Attr.disabled True, Events.onClick (Day19 PartTwo) ] [ Html.text "Day 19 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day20 PartOne) ] [ Html.text "Day 20 Part 1" ]
                , Html.button [ Events.onClick (Day20 PartTwo) ] [ Html.text "Day 20 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day21 PartOne) ] [ Html.text "Day 21 Part 1" ]
                , Html.button [ Events.onClick (Day21 PartTwo) ] [ Html.text "Day 21 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day22 PartOne) ] [ Html.text "Day 22 Part 1" ]
                , Html.button [ Events.onClick (Day22 PartTwo) ] [ Html.text "Day 22 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Attr.disabled True, Events.onClick (Day23 PartOne) ] [ Html.text "Day 23 Part 1" ]
                , Html.button [ Attr.disabled True, Events.onClick (Day23 PartTwo) ] [ Html.text "Day 23 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Attr.disabled True, Events.onClick (Day24 PartOne) ] [ Html.text "Day 24 Part 1" ]
                , Html.button [ Attr.disabled True, Events.onClick (Day24 PartTwo) ] [ Html.text "Day 24 Part 2" ]
                ]
            , Html.div [ Attr.css [ Tw.space_x_2 ] ]
                [ Html.button [ Events.onClick (Day25 PartOne) ] [ Html.text "Day 25 Part 1" ]
                , Html.button [ Events.onClick (Day25 PartTwo) ] [ Html.text "Day 25 Part 2" ]
                ]
            ]



-- Subscriptions ---------------------------------------------------------------


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        []
