module Day17 exposing
    ( solutionPart1
    , solutionPart2
    )


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        maxHeightForHorizontalVelocity =
            \hVelocity ->
                maxHeightOverallTrajectoriesWithFixedHorizontalVelocity input (Velocity hVelocity 1) 0
    in
    List.range (findMinHorizontalVelocity 1 input.xFrom) input.xTo
        |> List.map maxHeightForHorizontalVelocity
        |> List.maximum
        |> Maybe.withDefault -1


solutionPart2 : () -> Int
solutionPart2 _ =
    let
        countHitsForHorizontalVelocity =
            \hVelocity ->
                nrOfVerticalVelocitiesThatHitGivenFixedHorizontalVelocity input (Velocity hVelocity input.yFrom) 0
    in
    List.range (findMinHorizontalVelocity 1 input.xFrom) input.xTo
        |> List.map countHitsForHorizontalVelocity
        |> List.sum


approachZero : Int -> Int
approachZero value =
    if value < 0 then
        value + 1

    else if value > 0 then
        value - 1

    else
        0


type alias Position =
    ( Int, Int )


type alias Trajectory =
    List Position


type Velocity
    = Velocity Int Int


maxTrajectoryHeight : Trajectory -> Int
maxTrajectoryHeight trajectory =
    trajectory
        |> List.map Tuple.second
        |> List.maximum
        |> Maybe.withDefault -1


maxHeightOverallTrajectoriesWithFixedHorizontalVelocity : Target -> Velocity -> Int -> Int
maxHeightOverallTrajectoriesWithFixedHorizontalVelocity target ((Velocity h v) as velocity) currentMax =
    let
        newTrajectory =
            traceTrajectory target ( ( 0, 0 ), velocity ) []

        newMax =
            if trajectoryHasHit target newTrajectory then
                max currentMax (maxTrajectoryHeight newTrajectory)

            else
                currentMax
    in
    if v > target.xTo then
        newMax

    else
        maxHeightOverallTrajectoriesWithFixedHorizontalVelocity target (Velocity h (v + 1)) newMax


nrOfVerticalVelocitiesThatHitGivenFixedHorizontalVelocity : Target -> Velocity -> Int -> Int
nrOfVerticalVelocitiesThatHitGivenFixedHorizontalVelocity target ((Velocity h v) as velocity) counter =
    let
        newTrajectory =
            traceTrajectory target ( ( 0, 0 ), velocity ) []

        newCounter =
            if trajectoryHasHit target newTrajectory then
                counter + 1

            else
                counter
    in
    if v > target.xTo then
        newCounter

    else
        nrOfVerticalVelocitiesThatHitGivenFixedHorizontalVelocity target (Velocity h (v + 1)) newCounter


traceTrajectory : Target -> ( Position, Velocity ) -> Trajectory -> Trajectory
traceTrajectory target ( position, velocity ) trajectory =
    if trajectoryCompleted target position then
        position :: trajectory

    else
        traceTrajectory target (trajectoryStep ( position, velocity )) (position :: trajectory)


trajectoryStep : ( Position, Velocity ) -> ( Position, Velocity )
trajectoryStep ( ( x, y ), Velocity h v ) =
    ( ( x + h, y + v ), Velocity (approachZero h) (v - 1) )


outOfBounds : Target -> Position -> Bool
outOfBounds target ( x, y ) =
    x > target.xTo || y < target.yFrom


hasHit : Target -> Position -> Bool
hasHit target ( x, y ) =
    x
        >= target.xFrom
        && x
        <= target.xTo
        && y
        >= target.yFrom
        && y
        <= target.yTo


trajectoryHasHit : Target -> Trajectory -> Bool
trajectoryHasHit target trajectory =
    case trajectory of
        ( x, y ) :: _ ->
            x
                >= target.xFrom
                && x
                <= target.xTo
                && y
                >= target.yFrom
                && y
                <= target.yTo

        _ ->
            False


trajectoryCompleted : Target -> Position -> Bool
trajectoryCompleted target position =
    hasHit target position || outOfBounds target position


findMinHorizontalVelocity : Int -> Int -> Int
findMinHorizontalVelocity initialVelocity xFrom =
    if (initialVelocity * (initialVelocity + 1)) // 2 >= xFrom then
        initialVelocity

    else
        findMinHorizontalVelocity (initialVelocity + 1) xFrom


type alias Target =
    { xFrom : Int
    , xTo : Int
    , yFrom : Int
    , yTo : Int
    }


input =
    Target 230 283 -107 -57


sample =
    Target 20 30 -10 -5
