module Day25 exposing (Cucumber(..), Cursor, Seafloor, cellParser, findFinalPositions, mayAdvanceFilter, move, nextCell, performStep, rowParser, rowParserHelper, sampleString, seafloorToString, solutionPart1, solutionPart2, stringToSeafloor)

import Matrix exposing (Matrix)
import Parser exposing (..)


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        input =
            stringToSeafloor inputString
    in
    Cursor 1 input
        |> findFinalPositions
        |> .stepsPerformed


solutionPart2 : () -> Int
solutionPart2 _ =
    -1


type Cucumber
    = Eastfacing
    | Southfacing


type alias Seafloor =
    Matrix (Maybe Cucumber)


mayAdvanceFilter : Seafloor -> ( ( Int, Int ), Cucumber ) -> Maybe ( ( Int, Int ), Cucumber )
mayAdvanceFilter seafloor ( coords, cucumber ) =
    let
        nextCellOccupant : Maybe (Maybe Cucumber)
        nextCellOccupant =
            Matrix.get (nextCell seafloor cucumber coords) seafloor
    in
    case nextCellOccupant of
        Just Nothing ->
            Just ( coords, cucumber )

        _ ->
            Nothing


correctlyOrientedFilter : Cucumber -> ( ( Int, Int ), Maybe Cucumber ) -> Maybe ( ( Int, Int ), Cucumber )
correctlyOrientedFilter desiredOrientation ( coords, cucumber ) =
    case cucumber of
        Nothing ->
            Nothing

        Just someOrientation ->
            if someOrientation == desiredOrientation then
                Just ( coords, someOrientation )

            else
                Nothing


nextCell : Seafloor -> Cucumber -> ( Int, Int ) -> ( Int, Int )
nextCell seafloor cucumber ( x, y ) =
    let
        ( w, h ) =
            Matrix.size seafloor
    in
    case cucumber of
        Eastfacing ->
            ( modBy w (x + 1), y )

        Southfacing ->
            ( x, modBy h (y + 1) )


type alias Cursor =
    { stepsPerformed : Int
    , seafloor : Seafloor
    }


findFinalPositions : Cursor -> Cursor
findFinalPositions cursor =
    let
        afterOneStep =
            Cursor (cursor.stepsPerformed + 1 |> Debug.log "step") (performStep cursor.seafloor)
    in
    if afterOneStep.seafloor == cursor.seafloor then
        cursor

    else
        findFinalPositions afterOneStep


performStep : Seafloor -> Seafloor
performStep seafloor =
    seafloor
        |> move Eastfacing
        |> move Southfacing


move : Cucumber -> Seafloor -> Seafloor
move cucumber seafloor =
    let
        advancingCucumbers =
            Matrix.toIndexedList seafloor
                |> List.filterMap (correctlyOrientedFilter cucumber)
                |> List.filterMap (mayAdvanceFilter seafloor)

        seafloorWithAdvancingCucumbersRemoved =
            List.map Tuple.first advancingCucumbers
                |> List.foldl (\coords -> Matrix.set coords Nothing) seafloor
    in
    advancingCucumbers
        |> List.map Tuple.first
        |> List.map (nextCell seafloor cucumber)
        |> List.foldl (\coords -> Matrix.set coords (Just cucumber)) seafloorWithAdvancingCucumbersRemoved


seafloorToString : Seafloor -> String
seafloorToString seafloor =
    let
        cucumberToString : Maybe Cucumber -> String
        cucumberToString maybeCucumber =
            case maybeCucumber of
                Just Eastfacing ->
                    ">"

                Just Southfacing ->
                    "v"

                Nothing ->
                    "."

        rowToString : List (Maybe Cucumber) -> String
        rowToString maybeCucumbers =
            maybeCucumbers
                |> List.map cucumberToString
                |> String.join ""
    in
    Matrix.toList seafloor
        |> List.map rowToString
        |> String.join "\n"



----------------- parsing


stringToSeafloor : List String -> Seafloor
stringToSeafloor strings =
    strings
        |> List.map (Parser.run rowParser)
        |> List.map (Result.withDefault [ Nothing ])
        |> Matrix.fromList


rowParser : Parser (List (Maybe Cucumber))
rowParser =
    loop [] rowParserHelper


rowParserHelper : List (Maybe Cucumber) -> Parser (Step (List (Maybe Cucumber)) (List (Maybe Cucumber)))
rowParserHelper reversedCells =
    oneOf
        [ succeed (\cell -> Loop (cell :: reversedCells))
            |= cellParser
        , succeed ()
            |> map (\_ -> Done (List.reverse reversedCells))
        ]


cellParser : Parser (Maybe Cucumber)
cellParser =
    oneOf
        [ succeed (Just Southfacing)
            |. symbol "v"
        , succeed (Just Eastfacing)
            |. symbol ">"
        , succeed Nothing
            |. symbol "."
        ]


sampleString =
    [ "v...>>.vv>"
    , ".vv>>.vv.."
    , ">>.>v>...v"
    , ">>v>>.>.v."
    , "v>v.vv.v.."
    , ">.>>..v..."
    , ".vv..>.>v."
    , "v.v..>>v.v"
    , "....v..v.>"
    ]


inputString =
    [ "..>vvv.>.....vv....v>.>...>..>..v>v>>...v>v>.v.>vv..>vv.vvvv....v..v.vv>>v>...v.vvv..>..v>v.vv.v.vv>...>...v..>.vv>..>>.>>>>.....>..>>..v>."
    , "..v.>...v>....>.v.....>>v...v>.....vv.....>>>....vv.v.....>>>v..v>.>v.....v.>v..v.......vv.>v..>.>v..vv.....v....v.>..>>.....>v>v......v>v."
    , ".>>...v.v...v>v.v>v.v>>..>...v.....>>vv.>.....>v>>.vv..>v>v>.v>>.v..>.vv.vv.>.v.v>.v.....v>.v.vv>>.>..v>>.v>vv>vv>.....vv>....v>.v..>.vv>.v"
    , ">.....v.vv.......v>v>>.v....>vvv.>.v.>v>.vv..vvvvv..>v....>..vv...>>>v>>>>>>......>.>>..v>...v.>>..>>>..v>>.vv.v>>>>..>.....>v>v>...>...>v."
    , "v.v..v.>vv>.vv>.v>.>v.v.>v..vv>.v.v.>..v...vvv.v>.v.v..vv>>vv.>.v>.v.v..>....v.>.v.vvv>.v.>vvv.v.v.>vvv.>>.....v>>...v.vv.>..>...>...v.v.v."
    , ".v>..>..v>>...v.>..>>>.>>.vv>>.....>.v...v..v..v.>>v.>v.>.>...v..v>..v>>>.>vv>>>.>..>v.>.>.....v....v>....v..>.v.vv>vvv>..v>..v..v......>.."
    , ".>.>>.>v>.>>.>.v.>.v>>>v.....>..>>....vv>>.vv..v.v.v.>v>..>...vv>..v>..>...vvvvvv.v.vv.vv>....>v...v.>vv..>.>...v.vvv..v.v...v.>.>.v......>"
    , "..v.v.>v>vv.>v.v.v>...>.v>.v..>vv...........>v.>.v>..v>>>.>.>vv>v......>.>.vv.v>v.v>..>>..v..v.v..>vv>..vv..v..v>.>..vv>.>v..v.>.v.>vv.>v.."
    , "v.>.v.>>.vv>.>.>v.v.>v>..>v>.v.vv.>>v....>..v....>.v>>v>.>>vv...>>...v.v>>...v>>.vv.....>....vv.>>vv.>....>>>>v>.v..>v..>>>>.vv>>v>..>...v."
    , "vvv..v.>....>vvv.>>.>>.v>.>.v...>..>..>.>.v>..v.v.v>..vv>.>....v.>..vv.>...>.>>.>vv>..v.....>..>......v..v......v.vv.v.v.v>..>..>vvv.v>.>v>"
    , "..v.vv>.>>v...v.v......>.v..>.>.>..v.vv>.vv>>>.v..vv.>.>v...>>v.......v>.>>..vvv..v.v>vv...v>.v.v.>....v..>>vvv..>...>v>vv.v>>...vv....>.v."
    , ">>.>...vv.>.>>>.>..>..v....v.....v.>v.>.>.v>.vv.v>vvv..>.v>..v.v.v.>v..>>.v.>v..>.v>>>..>>.>.v.>>>.v.v..vv.v....v.>>v....v....vv.>>>>..vv>."
    , "...v..v..>>.v.>v>v>>.>v>>>v.v....>...v.>v.>....>>>...>vv>v>..vv...v.>vv..>>.>vvvvv>v...vvv..>v....v>.vv>v..>>>.v..v.v.v..v.v>.>vv...v..v>.>"
    , ".>>>>..>.....>v.>......>v..>>.......>vv...>>.....v>..>v..>..vv...>....>...v..v>..>.>v>>..v..>v>......>>.....v...vvv...vv.>..v.>.v..>vv.v.v."
    , "vv>>vv.>...v..v..>v.v....vv..>>v>>v....>v.....>.>>>.....>....>>v..>..v>.>>.v........>>.v>..v...v>>>v.>v>.v.v.v..vvv>v.>vv..>.v>>vv..>vv>v.v"
    , ".v.vv>.v.v>>....v>.....>......v..v...>>v>....v.>vvv...>.vv..vv..vv.>.....>v.....v.vv....>>>.v>.v.>.>v..>>......>>>...vv..>...>v>.v......v.v"
    , ">.>>>.>>.v>>.v..>....v>.v>..>>v....>v...>.vv.>.....v......>>..v.vv......v>>.....>vv.vv>.>..v>.>>v..>......>....v.>>.>>.>v>>>...v.v.vv.vvv.."
    , "vv.vv..>vv......>.>vv...>v.>v.vv.v>>v>>vv..v>..>......v.v.v>>..>.v.vv.>vv...v.>>>.v.vv..>>v.>..v..v.v...>v..v.v>....v..>.>>v..>.v.......v.."
    , "...>v.>..v...v..v.>..>v>.v>.vv>...>.v>.>>v>..vv>v....v..>>v.v>v.v>>......v>>..v>>>>.>..v..v.>.vvv>v.v.v.>>....vv.>.vvvv.v>.>>>.>>.v>..>vvv>"
    , "....v>>>vvv..v>v>v..vv..v>v.>>v.v>v>vv>..>..>>....v>...>....>vv>.v>.>.v>>>.v.v>....v>>>.>v.>>.......v....>>vvv>.v..v.>..v.v.vv>.>.>>>>.>v>."
    , "vv>v>..v.>>..>>..v...>.>..vvv..>.>v.vvvvvv..>.>>>...>v..>>>.>.>.vv...>...v>vvv..v..>..>vv.vv.>.>v.v......v>.>v>vv>>..>vvv..>v.v>v.v..v.v>.v"
    , "..>v>.v..vv>....>>v..>.........v>>..v....>>>..>>>>v..v...v>>v>.>.>>>...>>.>>...v.>>...>....v..>v.v....>..>.>v>>v>>.>>>>.v...>.v>v...>..>v>>"
    , "v...>.vvvv>.>v>v>>>>...>v...v.....v>.>v.>.vv.vv.>v.v>.v....vv..>.>v..v..v>>>.....>v..v>v...v>.>..v.......vv>>v>..vv.v...>.>..>.>>vv..>>v>v."
    , "..>..vv..v..vvv>.>.>v>...v>..>..v...>>>.vv.vv>>>.>....>>.>..v>.v>>v.>.v.>..>..>.....v>>v..>>>.v..>v.v.>.v>v>.v.vv.vv.vv...v.>>.>v.v..>vv.v."
    , ".>vv....v..>..>..vv>.v..vv.v....>>...>v.v.>..>>v..v...>>>>>v>.v...>..v.vv>.v.>.>.vvvv.vv...>.>vvv..>v..v.>..v>.v.>.v.>.>>..>..>vv....>v.>vv"
    , ".vvv.>>.v.>.>v>.>vv>v....>..v.v...........>.vv.>.v..v>>>>.v>..v.>..v>>.>.>..>v....>....>>v....>.>.v>.>.v.>>..>.>.>.v>.v>v.v.v.>.v..>>.vvv.>"
    , ">v...>..>..>v..v>v.v...>.>>..>.>.>..vvv..v....>v.v..>.v>.....>>.vv.>...>>>>v>..v..v>vvv.....>..>>v..>>v>>>...v...>.>>>>.vvvv..>vvvv.vv.>..v"
    , "vvvv>v>>.>v.>..>>...>>v.>.>...vv>vv..>..vv..v>..>..>..>vvv>v.>>>vv>...>v.>..v>v......>....>>.v..vv>>.>v..v..v..v>>>v...>>.vvv>v......vv...."
    , ">>v.....>v>...v..v.v>>..>>..vv..vv>.vv..>....vv..>>....vv>v>...v>>..vv.v>.vv.v....v.>vv.>.vv>>.v....>v.v>>v.>.......>.>>...>.v.>...>>>.>v>."
    , ".v.>>.>.v...>.v.>..v.>...>v>>v.vv>.>.....v.vvv.vv.v..>.>.v..v.v>.>......v....vv.>...vvv...v..>v...>>v>..vv...v>v..>.>..>..vvv>....>v.v.vvv."
    , "vv.vv.>..>..vvv.vv.v.>....v.v.>...vvv.>v....>>.>v..>.v>v.>...>vv.v.....v.>>....>.....v>>v..v.v......v>>v.>vv>vv>>>v.>....v.>.vv.v.>v>.>.>.."
    , ".vvv..v.v..>..>vvv>>>.vv>v...>...>>v.v.>..v>.>vvv..>.>v....>>.vv.v>vv.v.v>.>v..>..>.v.>>.>>>.v>...v...vv.>>.>....v>.v.vvvv.>vv...v>vv.v..>."
    , ".>>.v.>>>>>>>v.>..>..vvv>>.vvvvv.>.>.v>v..v...>..v..vv...vvv>......v>>.>>.>>.>.v.>.>.vv.vvv.vv..>>.>vv..vv..vvvv...v..>>..v..>....v>>...>.."
    , "v..>vvv.v...>v>.v>..v..v>.vv>v.v>>v...v..v>.>..vv...>v>v..>vv.>.v...>.vv>....v>..>>...>v...>>.v..>v>v..v.v...v.>v...vvv>.v..>.v>>>v.>v>v>.v"
    , ".>..v.>v>.....v.v>.>...>..v.v>.vv.v>>.v......v..v>v>v...v....vvv.>.v>......v.v>>>.>...vvv.v..>v.>.v.vv.>>.vv>v.>.>>>....v.>vv.>>...>>>.v>.v"
    , ">..v.v>.vv..v...>v...>v..v..>>>.>>....>.>>..>v>v..>.>..>>..>v.>.v..>.v>...v>>..>..v....>>>v>.>v.....vvvv.>v.>.....v>vvv.>vv>.>>v>v...v..v>."
    , "v>......v.>v..>>>.v>.v>..>.>>...v...vv>>>vvv>v>>.>..v.v.v>.>.>>..vv.>>>.>..v.v..vv>>.>>..vv.>>>>v>...>..>>>v>>>>..>.>>.>.v...vvvvv....v..>v"
    , ">..v>>vv.v.v>..v.v..v..v.>>>vvvv..vv>...v>.>..v>>..>.....>.vvv.vv.v>>>.v>>.>..>.v.v>..vv..>..v>v..>vv..v>v.vv>.v.vv.v>...v.vv...v>.......vv"
    , ">v.>v>>..>.>....>.....>..>>>v>..v....v>...>..>>.vv>vv>>.>>vv>...vv>vvvv>v>..v..>.v.>.....v...v.vv>v>.>v.v......vv..>...v>....v......v..>>.."
    , ".>.v>.>...>...>>..v.>.>..v>.>v...vvv.>v.>>.v.>.vv>..>..>...>.v.vvv..>..v.>..>>>..>>vv.v>v>>.>>>>v...>>>v>.>....vv.>vv.v>v.vv.>v>...v>>.>.v."
    , ">>..>>v...v.....>.vv..v>vv.vv>v..>.vv>...v>.v>v..v...v>.v.v>..>vv..vvvv>>>.>>>>.v.v.....v>.>...v.>..>.>.vv..>....v>....>.v>.vv>.vvv>vvv...."
    , "v.>>vvv>vv.vv>..v.>v....v.v>>..vvv.>..v.v>..>>>..v>..>.v.vv.>v...vv...>>.>>>>.>.>..vvv>>...>>v..v>..>vv.v>>.....v>>.vvv>.>>>..vvv>v>.>.v..."
    , "..>vv.vv.>..>>...>.....>.>>.v....>v>vvv...v.v..>.>....v>.v>..>.>>v...v.v>>vvv.v>....v>.>.v.>v>>>..>.v.>v..v>v.>.>v.>.>.>.>.>.vv.v>.>>.>..>."
    , "vvv........v>>...>>>.>.vvv.>>..>...vvvv.v....v>>...>v...>.v..v..>...vv...>v>>v.>>.>v.vv..v.v.>.>..v.>>>v.v..v...>v>..vv.>..>>>..v>v.v>...>v"
    , ">>.>>vvv.>>.>..>..v>..v.v>>.v>.>.>v.>>v>>..v>>...vv.>vv>.>v>>v>..v>vv>>..>v.>.v>v.>.v.v..>vv..vvvv..>>v.......>v.v...v...vvv>.v.....v.v..>."
    , "..v.vv.>....v..v>v.>.vv>.>>v>>>v....>v..v.........v>.>.v.vv.v>v.v>.>...v>v.>...>.v.>>.vv....v>..v>.vv.v.>v...>.>.vvvv.>...>..>>>.>vv..vvvv."
    , ".>........>..>v>.>>..v..>..>vvv.>.v>.vv.....vv..v>.v>v..>...v>v>.>......>>>..>>vv..>.v.....v...v>..v..v......>.v.>...v>.>>...vv.>.vvv.>.v.>"
    , ".v..>..>v>v>v.>.>>>.v>v.>>>>>vv.vv>.vv.>..>vv>..v.>.v..>vv>v...>....v>..>>....>>...>v..>>vv...>vv>....vv.....>.v.v>..>..>>>v..>.....>...v.>"
    , ".vv.>.>v>>v>..>v.v>.....>.>.v...v......v...v.>....v>.>>>.vv.>.v>..>.>....>>v>>v...v>>>...v....>.v.>....v.vvv>vv>.>>..>.vv.vv.....>..>>..>.."
    , "..>.>>.>.>.>vv..>..v...v.>.>v....v>>>....vv.>.vv.>...vv>....v.>...vv>v...>..>>....vv.>vv>>....v......>>..v>..vvv>>>>..>v>.>.....>.v...vv.>."
    , "vv>>...v.v.>.vv.....v...>vv.....vv>vv..vv..>......vv....>...>>..>.v.>>v.>v>..>>>.v...>...v....>>.vv.>.>vv>>v.vvv.vvv.>...v.v..>...v>....v>."
    , "..>.vv......v.v>.>..>..>>.v>.vvv...>..>..>>>..v>>>.>.>>..vv..v.>v.vv..v...vvv.vvv.>>>>..v>>>.v>v>.v...v>...>v>v..vv.>>>>>>.vv.>v>.>.>>>v.>."
    , "..v>vvv>>v.>.vv...v>vv>v..v.v>>v.>.....v>>..>.>.>>.>v....>.>v...vv>.v>>..........v>vv..>.>..vv.v.v>..v.v....v.v>........>>>>vvv>>.>.v>.>.>."
    , "..v.......>.v.v.>>.v......v.>.v.v.>v..v.v>..>v>..>v>>..>..vvvvv.>.>..>...v>.>>.v..v>vvvvv>>v>.>v.vv.v.>.>.>.v.>vv>>vv>>.v>.>>.vvv.>>>>.v.>."
    , ".>vvv.v..v>v..v>..v>v>..>..>v..v.>.>.>.>>..>...v.>..>.vvv.>v.>>...>v....>...vv.....vvv....>>vv>..vv..>..vv>...>>>>>..>..>.vv..>>>.v..>vvv>>"
    , ".>>.v.v>.>..>vv>v>..>v..>...>>..>v>.>..>v....v.>..>v.v>>v...v.v>.>.v.>>v.vv...>..>..>>..v>.v...>.>..v>>v.>..>...vvv..v.v>>v..v..>>....>>v.>"
    , "v>v...>.vv>..>.v...>>..v>>.v>.>....>.>>..>....vvv.>...>.>v>.v....vv.>.>..>>>.vv..v.>v>.>v.vv>.vvv.v....>v.>.v>>>>v.v..v......v.v>v>..>..v.."
    , ".v.>vv>..v.>>v.v.vvv>>.>..>>.v>>.>v.>.v..>..v.>.v>>....>>.v..>>..>v...>.......vv>v.v>vv...v>>..>....>.>v.v..v>.>.v.>.v>>>vvv..>....>.>>v..."
    , ".v>>>vv.vv..vvv.>.v.v.v>v.>v.>..>.v.....>v>..>vv.>...v>v.>>v.>>..>v..vv..vv>...vv.>.>vv.v.>>..>vvvvv.>.v..v.....v.>.>.>>..vv.>.>v.v.>.>vv.."
    , "v...v...>.>>v>v>..>>v..vv.vv.>..>v.v>>>v..>..>v.>..>v.>.vvv..vv..>.v.v..v..v>...vv>v..vv>.v>...>>v......>>>...v....v..vv.>>>v..v..v>...>..v"
    , "v.>.v.>.>>v>.>>.>.....>v.>..>.vv.v>.v....>.>.>...v>..v>.vvv>.>..v>.v...v.>>...v...>>.v.>vv..vvv>..>vv>>vvv...>.v.v.>.>..v..vv>vv.>v.>.v>..."
    , ">v>>...>vv..vv.v......>v>vv.v..>vv...>.v....vvv.v...vv.v...>.v>...>>.v.>.v....v>.>>>.>..vv....>..>v>..>.vv.vv>.v>v...v......vvv.v.>>v..>vv>"
    , "..v.>>>...v.v>....>vvv...v.>..>v.v.v....vv>vvv.v..>..v.>>vv>....>>.>..>.v>v>.>v..>>.>.vv....>v>>.vv..>v..>..vv..v>..v.v>v..>...v>.v.>>>vv.."
    , ".>v...v.>.v>vvv.v>>>....vv.v.v...v.....vvv..v>>>......>v>vvv..v>.>v>.v>.v.>v..>..vv>>vv>v..v..v......>..v.>..v.v.v.>..vv>.v.>>v>..>>vv>v.vv"
    , "...vv...vv>v.v>>.>v..v.vv..v>..>>>........v.>.....>>>.vv.>>>..>.v.v...vv..>>.v>>.v.v.>..v...>>.v.>>v.vv>v.....vv.v...v.>v>>>.>>.>>v.v>>v.>v"
    , ">..>.v....v>....v......v....v>..vv.......>>v>v>..vv>.>...>.>>v>..v.v>.v.vv.>v.....>>vvv.>....v..v....v..>..v.>>.>.>...>.v>.v>v>.>..>..>v..>"
    , "v..vvvv.>..>.v.v>.v>.>v>v>.>....vv....>......>v..>..vv>.>>.v>>v...>v...v..>....>v.>>..v..vvvvv>.....>.....v.....>>.>.vv.>>...v.v.>....v...>"
    , ".v.v.vv.>..>v>>...>vv>.>.v.>>.v>v.v.>...v.v.>.>v>v...v.v>.v>>>>>...vv.v.v......>v.v.v>v....v...>>..v.>...v..v>..v>v>.>v....>....v..>.>>>vv."
    , "v.v>.....>v>.>>.....v>vv.>.>....vv.>vv.vv.>vvv......v.>.>>>.v>vv....>>vv..>>>.vv>>>>>.>v......>v.>..vvv.vv..>>.v.v.>.>..v>..v>.v>>.....v>.v"
    , "vvvvv>.>....>>.v>...vv.v.>v.>>v>>.>...>..>.vvv....>....v.>.>>..vvv...v.vv.v>vv...>.>>vvv>vv.v..vv.v.>>.v.v>.v.>..>>vv...v.>.v..v>.>..v...>v"
    , ".v>>.>>>.vv>.v.v>vv..>.>.>v...v.>.v.>..>..>>...v>.v.v...v.>.>>vv...v.....v>.>v.>>>vvv......v..>.>v.vv.v...>>.vv.>v>v..v...vv>vv.v.v...v>>.."
    , ".v...>v.....v...v>.v>vv.>>v.....v..vv.>v........v..>>>.v>vv>..>...>v.vv>.v.vv>.>..v...>..v>vv.v>v..vv..vv.v>v....v>.>..vv.>>..v>.>..v..v.v>"
    , ".>v.>v.........>.v.v.v.>...>.vv......v>vv..vv...v.v>.v.>..v...v>v>>v>..>.v.....>.>>..>.>v..>.>>..v.v.....v..>>...vv>>.>.v>...>...>vv..v...v"
    , ".>vv.v.>..>v.....v...v.v.v.v>>.vv..>>v.>v.....>..>v..v.>vv.>>>v.vvv....>.v.>>.v>>v.v...v>v..>>.>..vvv>.>vvvv>..>.>v.v.v......>.>v..v.v.>.>v"
    , "...v>.....>.v.vv..v.vvv.>vv>>....v>vv...>v.v>.>>>.v..v..>vv>v..>.vv..>.>>v>>..>>>>>..vvvv.v>v>v.>.>...>>..v.>vv.>....>v.vvv>>....v.>>>....v"
    , ".v.vv..v.v.>.>.vvv>>vvv>vv.vv...>...v.>>..>.vv.v>vv...v..>....>>...vv...>.v>v.vvv..v..>.v>..>.....v>>.v...>..>.v..>.>..>.>.>.>.>.vv.>.>...v"
    , ">.>..v..v..v..vv...>vv>.vv....>.>.>.vv>...v>..vvv.v.v.>v>..v>..>.>v..vvv>v.>v>.>>vvv.....v.vv...>vv...>...vv>.vv>>>.>>>v..>.v....>>..vvv>>>"
    , "vvvv.>>.>.>v...>v.v....vv.v>vvv..v..>>v...>>.>>v.>>>v.v..vv...v>>.>>.>..>..v>.vv..vv>.vvv>v..>.v.v..>v...v.>v..>>..>>>>....>v.....>>.v.vv.."
    , "v.v.>>v.v>>..v>v>..>....>.>>>v.>.>.>..>>vv.>>>v>.v.>v>.>>v.vv>.v.....vv.v>.v.>.>.....>..v>..>v.>..vvv.v.vv>>>v..>..vvvvv...v..v...>.vv>.>v."
    , "v.vv.v>v...>>.v.v.v..v>vv...>..v>.v>...v.>..>..v.>v..>....>.v>vv>....vv.>>....>vv...>v...v>...vvv..>.vv..vv.>v..>>...>v..v....v..>..v>v.>v>"
    , ">.>...v.v.v..>>>vv....v.vv.>>.>v.>>>v>v..v.>.>...>v.>..v..v.......v>>...v>vvv.vvvv>.>.vv>>>v>.v..v..>>>..v..>...>vvv...v>>.>...v..v...v..>>"
    , "v...v>>.v.>vvvvv>.v>.>>.>.v....>v>vv>v..v.>v.>v..v>v>.v.>....vv....>..>.v>.>.>.....vv.>..v.>..v.v>v>v.>>..>.>.>...>>..>>>.>.>.>vv.vv.>.>v.>"
    , "v>.v..v>.v>>..>...v..>..v.>>>.>.>.v>>..v...>...v.......vv...>>.v..vv..>..>>>v..vv.v>>>v>.>..>..>.vvv.>v..>>...vv.v....vvvv>..vv.v.>.>v.>..>"
    , "..>>.....vv.v..v>.v..>.v.>>>>..>v..v...v>..v.vvv>.v>>>v>v.v.>>..v..>>.>>>.>>...>v...v....v..vv....v..v..>.>vvvv.v..>v..v.v>>>.>v>v>.vvvv>.>"
    , ".v>>.>.>v>..v..v>v..vvv....v>...>v..>v.vv.v>.v>..v.>v..>>.>...>.v.v.....v......vv...v>v..>.>.>.>vv........>>v.v..vv>v.vv...v>vv>.v.v>..>>v."
    , "vvv..vv..v>..>v.>>vv>v>vvv..>..vv>>.>.v.>.v>.>...>>v.....v..v.....>.>>...v.v>v.>>.>.>v.v>..v.....>>.v>>vv.v>...v>........v..>...>.>>>vv..v."
    , "..vv.v...>>..v>>..>>v>>..v....>.>.>.v.v.>..>.v>.v>.v>vv..vv.>..>....vvv>.>.v>>vv>v>>>.v>......>..v>v...v>.>v.>>>>>v>.v>>v..>v.>..v>>>v....>"
    , "..v.>.v>.>..v>..>....vv.v>v.v.>v.>v.v.v>>...v.>v.v>...v...vvv.v....>v>.v.v.>>.>v....>>>..>>vv.>.........v>.>...vvv.>>...>.....v....>v>>>>.v"
    , ">v>v..>.>.....v.>.v..v.v.v>.....v...>...>....>>v..>...v.>>v.>..>...>.vv>v>..v>..v.>>......>...>>>.v.....>>v>>>v.....vv>>.v>.vvv>.v>>>v...v."
    , "v>v>.>v...>.v>vvv......v>>>.v>...v.vv>v.>v.>..>..>.>>.v..>>v.>...v.v..v>>....>.v>.>>>...v>>v>>..>.>>...v>.>.vv>v.>.v>.>.v....>>..>.v.>v...."
    , ".>v.>....>..>>v...v>...>.vv........>.>v.v>.v.v..vvvv.......v..v....v.>.v>.v.>v....>vv.v>>...>v>..>.>..v.>..>.....v.>.v.v.>.....v.>v>...v.v."
    , "v>v.v>v.vv>...>v.>>..>.>>>.>.>v>...v.vv...v..>vv>.>>v..>.vv...>.v....>.v>.>>v....v........>...>.v>..>>vv.>........>....>....>...>.v..>>...>"
    , "....>..>...>..>.>...v.v..v>>....v>v..vv.....vv.>...>.>....v.>..>>>>..>.v>v.v.v>.....>>...>v.v.vvv..v..>...>.vv>vv.>>.>>vvv>>vvv>v>.>.>>.>v."
    , "v.v.>v>>>.v...vv>...>.vv.>>..v.v.vv..v..v>>vv>.>>>>.v..v...vv>...>>>...vv...v.>>.>.>.....>v........v>v..>.>.>>..>.v>>>>.v.vv..v...>.>...v>."
    , ".>..v.vv.v..v>>....vvv...vv.>vvvvv..v..vv>vv..>>>vvv>.>>>..v.v..v.v>v>..v>.v.v>..v..v>.v.>v.>>>v.>v.....>.>...>>....>...>v.v>.vv>v..>...v.v"
    , ".v.v.>v.>vv>>>.v>.>vvvv...>.....>...>..v>.v....>.>>.>.>...>..v....>vv.>>...>....>..v>.>vvv>..v>....>v.v>.v>>.>>>...>..>v..vv.vvv.>.vv..vv>v"
    , ".v>>v.>.>v>.v..>v.>v.....>vv>.v....>...v.>.>v>>..vv....v>v>..v....v>......v.>..v>..v>..>.v......v.....>v...v.>.v>.>.>..v...v.......v....v>."
    , "v.vvv.v.>.>.v.v.v.>>..>>v.>>.v.>.>.v.>.vv..v.vv>.v...>vv.>.v.v>.v.>.>..v>vv..>v.v.......v..v>.vv..>...vv>>>.vv..>.>.v..vvv.>v.....v.v>.v.v."
    , "..>>....>....>>..v..v.>v..v>....v>...>v...v.v.>>....v...vv>v.>...>>.v.>>>.>>.v....v>>>..v>..v...vv>>.v.>..>v..v.v>>..>.vv....vv.>...v..>v.."
    , ".....>..v>...>v.v..>v>..>v>..>>...>v...v.>..vv.>.>>v.vv>vv>..>..v>v.>>.>.>.v.>vv....>vv.v>.vv........v.....vv....v.v>.v.>>.>>.v.vv..v.>.>vv"
    , ">....>v.v...>>.>.>.>.>v..v.v>vv.>>.vv....>.>..>>.>vvv.>>v..vvvvv.>>v.>.>..vv.>....>>>.v..vv..>>>.>v.v>v.>.vv.v>.>v...vv>......vv.>v>v.....v"
    , ".v>.vvvv>...>v..>.....v.v..>>..vv.vv...>.>>.>vv...>>>.v>.>..vvv.>>vv..>...v..>>..v>.....v.>..vv..>>.v>...>.>.>v.v...>>..v.v...v..>.....>.>."
    , ".>>......>..>v.v>v>....>..vvvv..v>.v...>>>.......>vvv.....>>v.v.v.>v.v....>>.>>.>v........>....v.>v....v>>>v..>>>...>v.>>>....>........>..>"
    , "v>v>.v.vv.....v>vvvv.v.vv..v.v...v..v.>>.>.>.....>.vv....vvv..v>>>.v>.vv...>v.>...>vv.v>.v>vv.>>.>...v......>>v....>..v.>vv>......>..v>v.>."
    , "v..v>.>v.>.....>.>>>v..v.v>>.>>>v..>vv>.>..>..>>>>vvv.>..v.v>.v>..v.v>v.v..>.vv..vvv>.>....v.>v...v....>....v.>.vv.>..v.>.vv....>>.>vv.v..v"
    , "v.>.v>v>..vv.....>>>....v.>>.>.v.>..>>>..>v..>v.v....>>.>..vv>vv..v........>..>>>.v.>....v....>....>v.........>>>.>v.>>.>v...vv..>..v.>>v.."
    , "v.v>.>>.>vvvv>.v.v>v>.>v>..vv.>>>...v.>...>>vv.v...>v.>.>>.>v..v>.>vv>..v..........>.v.>v>.......>.....v>>v....vvvv.>v...vvvvv...vv..vv>>>v"
    , "...>>....>vv.v>.v.v>>.>..v.>.....>v..vv..>>.>vv.>.v.v..v....>>>>>.>..v.vvvv..v>.v>.>.v>.v..>v..v..v>v>..>>v....>.....>vv.>........v...v>..v"
    , ">>v.v>.v>.>.>..v.v...vv.v.>..>....>..v......vv..v....>>..>....>.>.v....>...>v..v.....>...v>v..>.....>.v..>...v....v..>v>.v>.....>v..>>vv.>v"
    , "......>...>.v.>...>>......>..vv..v..>.vv>.v.>..vv.vv...v.>.v..>.v>.>v>v.>v.>.>.>.>.>..>vv>..v>v..v.v>v>.v.>..>v.>..>v.>..>...v.>>>.>.v>>v.."
    , ">..v>..>....>>.v...vv...>....v...>v.v...>.vv...>>.>.vv....>>....>vv.v.>v...>>v.>...>.v.v.....>.v..>.v.>v..>>v>>..>..v......v>...vv...>..v>."
    , ".v>......>v..v........>>.>vv>>.v>..v..>v>.......>>.>>.vvv......v>..vvvvv>>.>...v.>>>>.>..v...>.v.v.v......v........v.>.>.>..>>v>>.....v>..."
    , "v.v.v...>.>v..>v.v...v..v>>>.v....>......v>.>..vv..v.>vvv>v.vv..vvv>>>....>.v.v.>.v..>..v.v>.v....>..v..v>v..>.v..>.>.>....>v..>v>>>..v>..."
    , "v...v>..>...v.vv..vv>..v.vv.v.v.>>..>v>.>...v.v>>.>.>.v.v>.v.>.>vvvv.v.v.>.v.v>>>>.>>..vv..>..>.vvvvv...vv.v>.v>>vvvv>v.v.....vv..>v>>..>.."
    , ".>.>..vv...vvv.v>>>vv>.v>>.>..v>..>v>.v.vvv.v.v>.v....>.vv...>....v>v...>..>..v>..v..v..>>vvv.>..v..>v>.....v.vvv>.>>..>..>>.vvv>v.>>.v>>.v"
    , "v..>>vvvvvv.v>>...v>>>...v>v...v>v....v..>>....v.v.v.vv...v>>v..>....>>.>.>>v.vvvv..v.v.v..>.>.v.v.>>v...v....>v..>.v..v....>..vvvvv..>...."
    , ">..v..v....v..vv.vvv.v..>>.>..v.v>>.>vv.......>vv>v.>.v...v>.v>.v......vvv.>>.vv.>...>.v.>..>v.vvv..vv>..>...vv>.>vv..vvv..v..>....>....v.>"
    , ".vv..v>..v...>v>vv..v.v..>.v.vv>....v>>..v..>.v..vv>..vv.>..v.v.vv..>.v..v.>>.>.vv.vvvv.....>v.v>v.v.v>v...>v.>v.>>..>.v>v.v>>.>.>vv>.v...."
    , ">.>..v...>>v....v.vv>v.vv.v>>vv>.>.>vv.v....v...>>..v.>..>.v..v.>.....v..v.v..v..v>..v.>..>.v....>>.v..v>.>...v..vvvvv.vv.>.>.>>......v>.>v"
    , ">v........v...>.....v>..vv>v.v.>.>vvv..v.v...v>vv.>....>v.vv>..vvvvv.>....>.v>.v.vv...>v...>>v.vv....v...vvv>.v..>vv>.v>>....v....>.>v>.v.."
    , "vvv.v>>....v.>.>.>>....v>v...vv....vvv.v..>....v.>.vvv..>>.v..>.>...>vv....v>.vv>>.>...v>>....v.>....v..>.>.>...>v>v.v>.v..>v.vv.>.>..>...v"
    , ">..vv>v>...>..v..>>.>v.v.v...>>v>.vv>>..>..v..>.vv.>.vvv.>>.v>...>>>..>....>>>..........v.>..v..vvv.v.>.v..vvv>>.v>...>>>.>>vv..>v.>....v.>"
    , ".v>v..v>..>.v..v.>..>.v.v.>.v.vv>..>vvv>>.>.v...>.v.vv..vvvv>....v..v>>..>v....v>v.>.>....v>...vvv>....v..>..>>>.>..>v...v.v>.>.vvv.>.>v.v."
    , "..vvvv>v.v>>v.>>..>>v>>>>...v..>>..>.>v>...>.....>.>..>.v.vvv..>>v.>v>>.v..>v.>.>v.v.>......>>...>>.>>>...v.v>.>>v.>v..v...vv>v.....v.v...v"
    , ".>..v>...vv>.>.v.>>.v..>.>v.>.....>....v>v>>......v>..vv.v>..>..v.v.vvv>>.v...v.v>..>....v>>.>>.>..v..v.>v..vv>>.>vv..v>>v>v.>.v>>........v"
    , ".>>.>.v..>..>.v.v>..>vvvv.>>v>.v.>>v.v>v>>>v....v.v..>.v>......>v>...v...>v...v.v>...v..>....v>>....>.v>..v..vvv......vv...v.v.>.>>v>.>...>"
    , "....v.>>..v.v.vv....>.vvvv.>....>>.>>.vvv>.....>.>v>..v.vv...v.....>..v..>>.v..v>>..>>>>vvv.v>.v>.v..>.v..v>v.>.v.v>>>..v...vv>..vvv>...v.>"
    , "v>.>.vvvv.v.v>>.v.vv.....>v>....>v.>>...v.....>>vv.v.>.>.vv...>v..v.v>>....>..>>vv.>...v..vv>..vvv>..vv.>>v>.>.vv>v>.>.>.>.v.>v.>.>.v..>v.."
    , ">.>>v>v>>v>.....>.>..v.>..>>v.>..v.>.>.v.>.>>..>.....vvv>vv.>.v>v>>v....vv>.>.>v>.>>..v>v.v..>..v..v.....v......v>v>.v>.>v>>...vv>>>>..>.vv"
    , ">...>....vv>.v.>....>..>v..vv...>>v..v.>v...v.v>>...>...>.>.>.>....v.>v>>..>.>.vv.v>...vv>>>..>..>...>>..>....vv.v.v.v>>vv...>vv..v.>v.>.>."
    , "v....v...v.>..>>>.>..>.v>v.v.vvv>.>.vvv>..v..>....v.vvv...v.v.vvv>...vv...v..>>.v..>v..v>.>>>v>>>>>v.v.......v..>>>>v..v.vv>v.>vvv>>v...>>>"
    , ".....>.>>v..v>>..v.v.>.>..>.v...v.vv...>vv.v.v..v>.....vv.v>..v.>....v.v..v..v>.vvv..v.....>...>v>..>v...v..v.>v..vv.v>...>vv...v>.v...>..v"
    , ".v.>v..>..>v..v>..v..v.v..v........v....v..vv>.>..>>..>>.v.>vv..>>>v..>v.vv....vv...>v.vv...>>>v.>...vvvvv.>>.v..>..>>v.>..>v>...>.>>vv.vv>"
    , "...>.>v.v..vv>.v..v.>v.v>>.>..>v>>>.>...>>.>vvv.v...>v..v.vv>.>..>.>.v.>..v>v>.>>>>>.>...vv.v.......v>v>v>..vvv..>v...v.>...v.v.v....v.>..>"
    , "..vv..v..vv>.>.>vvv>>>>>v..>..vv.vv..>v..>>..v>vv.>v.....>.v..>>.>..>.v.>v>.>..v>>...>v.v.vv....v.v.>..>.>v..>>vv.vv.vv...>>....v..>.>>.v.."
    , ".vvv.v>.>..v..>..>v>.v..v.>.>.v..>>>>>.>...>v...v.v..v..>vv.>v......>.>.>>v>v.v..>..v.>.....v>v>>..>.>.>......>>.>>..vv>.v>..v.>>..>.>>>>.."
    , "v.v>vv..vv..>...vvvv.v.v>..>.vv>.........>>>.>..v>.>>>v>>>>>....vv>.v.>....v..v.v.v..>>..vv>.>.>.>vv>.v...v>.v.v.v.>>...>>.v>....>..>vv>vv."
    ]
