module Day21 exposing
    ( solutionPart1
    , solutionPart2
    )


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        finalGame =
            playGameTillTheEnd (input |> Debug.log "initial game") |> Debug.log "finalGame"
    in
    evaluateGame finalGame


solutionPart2 : () -> Int
solutionPart2 _ =
    0


type alias Player =
    { position : Position
    , score : Int
    }


type alias Position =
    Int


type Turn
    = Player1
    | Player2


type alias Game =
    { player1 : Player
    , player2 : Player
    , die : DeterministicDie
    , nrOfRolls : Int
    , turn : Turn
    }


evaluateGame : Game -> Int
evaluateGame game =
    if gameFinished game then
        if hasWon game.player1 then
            game.player2.score * game.nrOfRolls

        else
            game.player1.score * game.nrOfRolls

    else
        -1


hasWon : Player -> Bool
hasWon player =
    player.score >= 1000


gameFinished : Game -> Bool
gameFinished game =
    [ game.player1, game.player2 ]
        |> List.any hasWon


walkAmount : Int -> Position -> Position
walkAmount amount start =
    1 + modBy 10 (start + amount - 1)


advance : Int -> Player -> Player
advance totalRolled player =
    let
        newPosition =
            walkAmount totalRolled player.position
    in
    { player | position = newPosition, score = player.score + newPosition }


switchTurns : Turn -> Turn
switchTurns turn =
    case turn of
        Player1 ->
            Player2

        Player2 ->
            Player1


playGameTillTheEnd : Game -> Game
playGameTillTheEnd game =
    if gameFinished game then
        game

    else
        playGameTillTheEnd (playTurn game)


playTurn : Game -> Game
playTurn game =
    let
        ( newDie, totalRolled ) =
            threeRolls game.die |> Debug.log "three turns result"
    in
    case game.turn of
        Player1 ->
            { game
                | player1 = advance totalRolled game.player1
                , die = newDie
                , nrOfRolls = game.nrOfRolls + 3
                , turn = switchTurns game.turn
            }

        Player2 ->
            { game
                | player2 = advance totalRolled game.player2
                , die = newDie
                , nrOfRolls = game.nrOfRolls + 3
                , turn = switchTurns game.turn
            }


threeRolls : DeterministicDie -> ( DeterministicDie, Int )
threeRolls deterministicDie =
    let
        firstRoll =
            roll deterministicDie

        secondRoll =
            roll firstRoll

        thirdRoll =
            roll secondRoll
    in
    ( thirdRoll, firstRoll + secondRoll + thirdRoll )


type alias DeterministicDie =
    Int


roll : DeterministicDie -> DeterministicDie
roll deterministicDie =
    1 + modBy 100 deterministicDie


input =
    Game (Player 4 0) (Player 5 0) 100 0 Player1
