module Day20 exposing (EnhancementRules, Image, cartesianProduct, countLitPixels, enhanceOnce, imageToString, inputEnhancementString, inputImageRows, kernelCenteredAt, kernelValuesToNumber, rowsToImage, sampleEnhancementString, sampleImageRows, setPixel, solutionPart1, solutionPart2, stringToEnhancement)

import Array exposing (Array)
import Matrix exposing (Matrix)


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        enhancementRules : EnhancementRules
        enhancementRules =
            stringToEnhancement inputEnhancementString

        originalImage : Image
        originalImage =
            rowsToImage inputImageRows
    in
    countLitPixels (enhanceMultipleTimes 2 enhancementRules originalImage)


solutionPart2 : () -> Int
solutionPart2 _ =
    let
        enhancementRules : EnhancementRules
        enhancementRules =
            stringToEnhancement inputEnhancementString

        originalImage : Image
        originalImage =
            rowsToImage inputImageRows
    in
    countLitPixels (enhanceMultipleTimes 50 enhancementRules originalImage)


countLitPixels : Image -> Int
countLitPixels image =
    image
        |> Matrix.toList
        |> List.concat
        |> List.filter (\char -> char == '#')
        |> List.length


cartesianProduct : List a -> List a -> List ( a, a )
cartesianProduct xs ys =
    List.concatMap (\y -> List.map (\x -> ( x, y )) xs) ys


kernelCenteredAt : ( Int, Int ) -> List ( Int, Int )
kernelCenteredAt ( x, y ) =
    cartesianProduct
        [ x - 1, x, x + 1 ]
        [ y - 1, y, y + 1 ]


kernelValuesToNumber : String -> Int
kernelValuesToNumber string =
    let
        charToInt : Char -> Int
        charToInt char =
            case char of
                '#' ->
                    1

                _ ->
                    0
    in
    List.foldl (\char -> \currentNr -> currentNr * 2 + charToInt char) 0 (String.toList string)


setPixel : Int -> Image -> EnhancementRules -> ( Int, Int ) -> Image -> Image
setPixel convolutionIndex originalImage enhancement ( x, y ) outputImage =
    let
        borderValue =
            if modBy 2 convolutionIndex == 0 then
                '#'

            else
                '.'

        enhancedPixelValue =
            ( x, y )
                |> kernelCenteredAt
                |> List.map (\coords -> Matrix.get coords originalImage |> Maybe.withDefault borderValue)
                |> String.fromList
                |> kernelValuesToNumber
                |> (\value -> Array.get value enhancement)
                |> Maybe.withDefault '.'

        outputImageCoordinates =
            ( x + 1, y + 1 )
    in
    Matrix.set outputImageCoordinates enhancedPixelValue outputImage


enhanceOnce : Int -> EnhancementRules -> Image -> Image
enhanceOnce convolutionIndex enhancementRules originalImage =
    let
        ( w, h ) =
            Matrix.size originalImage

        outputPixels =
            cartesianProduct (List.range -1 w) (List.range -1 h)

        initialOutput =
            Matrix.repeat ( w + 2, h + 2 ) '.'
    in
    List.foldl (setPixel convolutionIndex originalImage enhancementRules) initialOutput outputPixels


enhanceMultipleTimes : Int -> EnhancementRules -> Image -> Image
enhanceMultipleTimes nrOfEnhancements enhancementRules originalImage =
    List.foldl (\enhancementIndex -> \image -> enhanceOnce enhancementIndex enhancementRules image) originalImage (List.range 1 nrOfEnhancements)


type alias Image =
    Matrix Char


type alias EnhancementRules =
    Array Char


imageToString : Image -> String
imageToString image =
    image
        |> Matrix.toList
        |> List.map String.fromList
        |> String.join (String.fromChar '\n')


rowsToImage : List String -> Image
rowsToImage rows =
    rows
        |> List.map String.toList
        |> Matrix.fromList


stringToEnhancement : String -> EnhancementRules
stringToEnhancement string =
    string
        |> String.toList
        |> Array.fromList


sampleImageRows =
    [ "..............."
    , "..............."
    , "..............."
    , "..............."
    , "..............."
    , ".....#..#......"
    , ".....#........."
    , ".....##..#....."
    , ".......#......."
    , ".......###....."
    , "..............."
    , "..............."
    , "..............."
    , "..............."
    , "..............."
    ]


sampleEnhancementString =
    "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#"


inputEnhancementString =
    "####....##..####..##.#.#.##..##...##.####....####.##..#.######.#####..##.#..#..#..###.#..###........#...########...#.#........#...####...#...##...####..###..#..#..##..#####...##.######...###.##.##..#...##....#.#...#.######.##.##.#..#.#..#..####.#####....#.##.####..#.##..#...##.#.#####.#..##......###.#..###.##.#.#..##...###..#..#..#.###...##.#....#.....##...##..#.##.#.#.....###..#.#...####..#..##.####...###...##...###..#....####..####..#.###.###.##.#.#.#.###.##.#.#.#..#.#...##...#.#.##..#..###.##..##.###.##."


inputImageRows =
    [ "#.#....#...#..#.....###.....#...##.#####.#.#.##.#.###.###.##.##.........########......##...#....##.."
    , "..#.#..#...#.###...#..#..#.....###.#.#.##.#.##.#..###.###.#..#.##.#.##.#.#.#...####.#.......#..##..."
    , "##..##...#.#########.###..#####..###.###.####....###.#.#.###.#.#..#..#.#...####..###.#.##.#####.##.."
    , ".#.##....##..#.###..#....#.#..##..####.#..#.#####.#.#...#.....#.##.#.#.#...#..#......#.#.#.##..#.#.#"
    , "##..#.##.#.#.#.##....####.#.#.....#.....#....#....#...###..###..######.#...#.#.....#.#.##...##..##.."
    , "###..#......##.....#..##.#.#...#...#.##..##..###.####...###.#...##.#.#.#.##.#....###...######...#.##"
    , "...##.#..#.#.#.####.####..#.#.#.##.#...#.....#.#..#.#####..###.#...#..#...####...#....##..##..#....#"
    , ".#.#.....##...#.##..#.##..##.#######.##..#...#..#.######.#.##..#..#......###.#..##.#.#...###.##..#.."
    , ".##.#######.##..#...#..###.###...##.##..#.#...#..#.#..###.##.#..#.##.#.#.##.####..#..###.###.##.#.#."
    , "#..###...#.####.#...##.###...##..#.######..#.#........##.#...#..#.##.#....#####...#..#.#.##.#....###"
    , "#..####.#.#.##..#.#.##...#...###....#...#.....###..##...##.#.##.#....##..##.##.#####.#.#.##.###..##."
    , "####.####..#.#..###.#..#####......#...####.##.#..##.##..#...#..#.#####...#...#...###.##.##........##"
    , "#.....###..#.###.#...##.#####...###...###.#.##.###.###.##.####..#..#.....#...##.#.....#.##..#..#...."
    , ".##..#.######..#.#.#.....#.#..###.#.####.##.#...##..#..#.#.##..#.....#.####.#.#.#...##.##..#......##"
    , "#.##...###.##.##.#...####.#..#...#.#..#.###.#.#.##.##..###.##.##.#..##..#.#.#.#..#.###.#...###..#.##"
    , "###...#.#.##.#.#.#.##.#.###.##..#.#.##..#.##..####.##..#...##.#.###....###..###..###.#..#.##..#.#.##"
    , ".......###...#####.#####.#.####..#....##.##.###.#.#.#.#.#...#.#.###.####.#..###.####.#.#..#..#######"
    , "#.#........###..###..#...####..##.#..####....#.#...##.#....#..###..##......#..###....###.#.###...#.#"
    , "...#.#....#...##..###.##.####.....####.........#..#..#.#.#.###.#..##.####........###.###.#..#.#...#."
    , "##.######...###.....#.##..#..##..#.#.#..#...#.#...##.#.##...######.#.#...###..#.########.#.##.###..."
    , "##..#..#..#.##...#.##.........##..#####.#.##.#.####...#.#.#...#...##.####..#...######..#.....##..#.."
    , "...#..#.###..#.##.###...#....#..#...#.###..####.#..######.##..#....##...####...##..#.#.#.###.###...."
    , ".##.###..####.##....#....######...#..####.##...###.##.#.#.##..##.....####.#..#.###.##...#....####.##"
    , "####.######.#..#...###.#.#.#.....####....#.#..#....##...##.#######.#..#..#.######.##......#...##...#"
    , ".#####..#..##...###.###.#.#....###.###.##...#.#...#..#..#..#.........#.##..#.....#....####..####.###"
    , "#..##.#.###.#......##...#.###..##.##.#.#..#.#.##..###.#..........#..#...#.###.###..###.#.#..#..#...."
    , "......#.####..#.####..#....##..####.##..#....##.#.#........#.#####..###....###..###.#..#.#.#.#.##.##"
    , "#..#.....####.#.##.#..#..#...#.#..##..##.#.#.####..#...#..#.#.#.#....#.###.###..##.#.#...###.###.#.."
    , ".##..#..####.##.#...####...#.#.#.#.#.#..#.#...##..#....#.#..#..#.#.##..#.#..##..#.#.#...##..###.##.."
    , "..#.#..#.....##.###.##.#####.......###..##.##..#..###..##..#.....##.#.#.....##.#########.####.#.####"
    , "##....####..#....#.########...##.###....#......#..###..###.#.####.##.#######.#############...#.##.#."
    , "##..##....#..#..#...#.#.#.##..##..#.###....##..#..##.#.#...#.....#....#..#....#.###.#...#...#.##..##"
    , "##.#..##..#..###.##.....#..#.##...##....#.#..##.#.....#.#...##......##########.....#....#.####......"
    , ".###.#....##..#..##..#####.###.#.##.#.####..###.#.###..#.##....####..#.#...#.#.#..#####...####.##..#"
    , "....#..####..#.......#.#.......##...##.#.###..##.........##..##...##.#..###..#.#....#.####.#...#.###"
    , "#.#.#.####.#.#.#.#....##.#...#..##.#.##..##.#.##..#.#..#.#.#..#####..###.#..#..#.#..##.#...#..#..#.#"
    , ".#..##....##.#...#.......###.#####.#........#.###.###.###..##.###...#.#....#...####.###.##..###.##.."
    , ".#.##.##.#.#..####.#..##.###..##..##.##.#.#####..#..#.#..#.##.#.####.#.##.#...##....#.....#.####.##."
    , "##.##.#.#########.#....##.##.#####....#..#..###.#..##.#..##.#.#.#######.##.#......##.##..#.####...#."
    , "#.....#.##...##...##.#.#...###.#####.#.##.####.##.#..#..##.#####.##..###.......#.#...##...#..###.#.#"
    , ".#.#####.##.##.#.#..##...#..#.#####...#.##.####.##.#.#.##.#.##.#..#....#.....###..##.##.##.#.#.#.###"
    , "##.##.#.#.#..#..#..##.#####.#...##.#...##..#.#..#..#.#...#.###..###.#...#..##...###.......#...#.#.#."
    , ".#.....#..##......#.#..###.#..#.##..##.#.....##.##...#....#.##.#......#.#..#..###..#.#..####.##..###"
    , "...###.....#.##..####.##.#.#...#.......##..###.###..##.#.#....#...#.#..#.###.####..###...#....#...#."
    , "..#.....#...###.##.##.#....#.##..#.#####..###.####.##..#.##.##.##..#.#.###.#.#.#...##....###.#.##..."
    , ".#########...#...##..###.#.###.#...#...#.###.###..###....###..#.##...##.#.#...#.#.####..#..#...#####"
    , "##..##..#..#....#....#####....###...#.##.###.#.#.#.....#..##..###.#.#..##.#.#..##....#.###.##..#..##"
    , ".....#.##...##......#..#.####....#.##.#####...#..#..#.###..####..#.####.##..#.####.#.#.....###..#..#"
    , "###....##.#..##....#####..#.#####.####....#....##.......#####.#...#.....#..#.###...##.#..#.###..#.##"
    , "#.##..#...##...##.#.###.###.#.#...#...#..#####.#..#.###.#.##.###.####...#.#.##..##.#.######.###..#.."
    , ".#.####...##.###.#.....#.##########.###.#####...###.#...###.#....#..##..##....##.##....#.##...#.##.#"
    , "###..#.#######.##.#####..######......#.#..#.#..#....######.##.###.###.#.##.#.##..#..##..###..##..###"
    , "....###.######.###.....##...#..#...##..###..########.#.##...####....##.###.#.##..##.#.....##.#.###.."
    , "#...#....#..##.#.#.#..#..#.#...#.#..#.###.######.##.####....#.##....##.###.##.##.......#.##..#.##..."
    , "###.#.#.#...#.#.#.#.#..#.####..##..##.##..##.#.##..##.###.####.#####..#.####...##.....#.#.##...#.##."
    , "###.....#####.#...#.#....##...#..####...##.##.#..#.....#.##.#....######.##.##..#.#..#####.#...##...#"
    , ".#..##.###.###..#######.#.#.###...##.###.#.#.###.#.#####.##......#.##.....#..####.#.##.##.....#..###"
    , "##....##.#.##.#..#.##.###.##.####..#.##...#.#.##..##.#.#...##.#.#.##....#.#.#.....##....######.#.#.#"
    , "...#.##.#.#.#....#.###.#.###...##....#..#..##...#.##.#.....##.#.###...###...#.#...###.#.##.##..##.##"
    , ".#.#..#..#....##....#######..#...##.##.....#..###.##.#..#....#..##.##.#...#####.#....##....######.##"
    , "#.#.##..##..###.....#.##.##.###...###.##.#.#...#....####.......#....#..##.##...##....#...#.####..###"
    , ".#.##..#.....######.##..##.###....#####..#...#.####..##..#.#.###...####....#.##....###.##..######..."
    , "..###.######..###.....#...#.#..##....##..#...######.#.#.#...###.##....#.#..####.#######..##.....#..#"
    , "#...#.#####...#.###..#....#.#....#..#...##.#...#.#..#.##..##..#..##..#.#.###.#.#.##.#.##.......#####"
    , "###..#.##.#.#.#.....##.##.###..####....##..#..#..#.###..##.....#.....######.....#.###.#####.##.#####"
    , ".##..#.#.###.##.#.#.#..#......##...#..##.###..#.###.#.#.#...##.###...#.....#.##.#.#..##.##.#....##.."
    , "##.#..#.#####.##..#.#.##.###....###.......###......#..#...###.##.#.##########.##.##.##.###.#.##....."
    , ".###.#..#####..##..#.####..#.#..######....####.###..#.##.####..#..#...##.#.#.####.####.#.##..####..."
    , "###.###.#.#.#..##..###..#....##.#..##.###.##.#.#......###....####.#......#.#.#...#.....##......####."
    , "###.#.####.######..###..##.#.#....#####.#..#....#.##.##..#####.#......#...##....#..#.#.##..####.####"
    , "#..#...####..####.####.#..#.##..#.#.#.####..#.####...##....#..####.#.#........#.##......#.....##.#.#"
    , "..#..#.#####.#...#.###..##.#..#..#..#...#...#...###.#####.#.##..#.######...##.###....#######....#.##"
    , ".#.#........#.##.#.######.###.######..#..##.##.#...#.###.#..####.#.##.######.###..###..........#..##"
    , "..###.#.#..#.####...#.##...#....##############.#.#..####.###.#..##...#####.#.##.#..#####.#..#...###."
    , ".#####.######.##...#.#.#..###.##..###.#.##.##....####..#.###...#.#.#...#..#..#.#..##......#..##.#..."
    , ".#.#..####.#.###.##..#.#.#.##..##.#..#.########..#..###...##....###.##.#...#.#.#.##.#..#.#..#..#..#."
    , ".##...#.#.#####......##.####.####...#.#######..#..#...#..#####.#.........#####..#.###.#.#.##...##..#"
    , "#..#.#..#.#..##.#.##.###.#..##.#..#..####.###.##.########.#.##..#.....#..#....#.##..#..######..#####"
    , ".#.#...####..##..####....#.#.###.#.#...##.#.#..####.######..#.#.#.#...#..##...#....#####....####.##."
    , "###...#.#..##..#.#..###.###...##.#..#.###..#.###..#.#...#.#..#.#..##...##..###.#.#....####..#....#.#"
    , "##....#.###.####..#.###.#..#######.###.....###...#.##....######.#.##..#####...###..#....##...#..###."
    , "###....####......####.##.#..#...###.##..#...##..#.##..####..###.###..#..#..###..#.###...#.####..#..#"
    , "#...#...##..#...###...#..#####...#####.##...#..#.#####.....#####.......##..####....##.....##..#..##."
    , "####..####.##..#.#..#####...#.#..##.#..##.###..#..###..#.....##.##..#..#.####.#..#..###.##..##..##.."
    , "####..#......##.#.#.#....#..###..####.##..##.###.##.....#......#.###...#...###.#..#..#.###.##.###..."
    , "..##.####..#..........###.#..##.####..#..##.##..##.####.#.##.####.....#.#.#..####.#####.##...#..####"
    , "..#....#..#.....#.##.#####.####...##.#.#.##..###..#.######.#.....##.######..#..#.##.##.#...#####.###"
    , "........#.#.....#.#.###...#######.#........#.#####.#.####...##.#.##..###...#......#.##.#.#.#..###.##"
    , "....#..##.#..#.###.....#...#..###.####.###.##..##...#.#..#....#.#.###.##.#..#...##.##..##..####..#.#"
    , ".#.#.##...#...#...#.#.#.####.####.#...#.#....#....##..#......#.#####.#.##.##.##.###...#.#####...#..#"
    , "###..##....#......########.##.##.##.##.#....####.###..#..##.##...#..##.#...##..##.####.####.##.#..##"
    , "#......###.###.##.....#.........#....###.#...##.###......##.#.....##..###.....##.##.###....##.#.#..#"
    , ".##.#.#.##.....#..#.#....#####.#.#...#.##..###.#####.###..###.##.....#.#...#..###..##..#...#.#..##.."
    , "########.#.#.##.#.##..#..#.#.#..#.###.#.##.######..#.####...##.##....###..#....#...#..#.#...##...#.."
    , "#....#.#####.#....######...##.#.###.#..####.##.###.###...####..#.#.##...#...##...##....####.#......."
    , "..###..#..####.##.#.#####...###.#...###...#.#..##..##...##.##.#.##.#.#...##.....#..#.#.######.####.#"
    , "#..###.#..#.#..#.#.....##.#.#.#.#....#.#..#..#.###.#.##...#...##..###.#...#...#######..####..#.....#"
    , "#..####.#####....####.#.#.#.#.##.#.#...#####...##.###..###.#.#............###.####..##..##.###..###."
    , "...#..#..#...#.#######.#.##..##..#..#.#..###.###..#.#####....#..#...####.#..##..###.##..######.##.##"
    , "##.#..#....#.#..###...#.#########.#####..#...#.####..#.#..#######.#....#.#.#.#..###.#.#..#...#.####."
    ]
