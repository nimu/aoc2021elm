module Day12 exposing
    ( input
    , solutionPart1
    , solutionPart2
    )

import Dict exposing (Dict)


type alias AdjacencyList =
    Dict String (List String)


isLarge : String -> Bool
isLarge cave =
    cave
        |> String.toList
        |> List.all Char.isUpper


isSmallIntermediate : String -> Bool
isSmallIntermediate cave =
    not (isLarge cave) && not (List.member cave [ "start", "end" ])


neighborsOf : AdjacencyList -> String -> List String
neighborsOf adjacencyList cave =
    Dict.get cave adjacencyList
        |> Maybe.withDefault []


solutionPart1 : () -> Int
solutionPart1 _ =
    let
        adjacencyList =
            stringsToAdjacencyList input
    in
    completePath adjacencyList [ "start" ]
        |> List.length


solutionPart2 : () -> Int
solutionPart2 _ =
    let
        adjacencyList =
            stringsToAdjacencyList input
    in
    completePathPartTwo adjacencyList [ "start" ]
        |> List.length


type alias Path =
    List String


endingCave : Path -> String
endingCave path =
    path
        |> List.reverse
        |> List.head
        |> Maybe.withDefault "ERROR"


caveIsEligibleForVisit : Path -> String -> Bool
caveIsEligibleForVisit path cave =
    isLarge cave || not (List.member cave path)


caveIsEligibleForVisitAtMostOneSmallCaveTwice : Path -> String -> Bool
caveIsEligibleForVisitAtMostOneSmallCaveTwice path cave =
    let
        increaseCounter : String -> Dict String Int -> Dict String Int
        increaseCounter c dict =
            Dict.update c
                (\maybeCount ->
                    case maybeCount of
                        Nothing ->
                            Just 1

                        Just counter ->
                            Just (counter + 1)
                )
                dict

        containsDuplicates : List String -> Bool
        containsDuplicates caves =
            List.indexedMap Tuple.pair caves
                |> List.foldl (\( _, c ) -> increaseCounter c) Dict.empty
                |> Dict.values
                |> List.any (\v -> v > 1)
    in
    isLarge cave
        || not (List.member cave path)
        || isSmallIntermediate cave
        && not (containsDuplicates <| List.filter isSmallIntermediate path)


appendToPath : Path -> String -> Path
appendToPath path cave =
    List.append path [ cave ]


completePath : AdjacencyList -> Path -> List Path
completePath adjacencyList path =
    let
        lastCave =
            endingCave path

        eligibleNeighbors =
            neighborsOf adjacencyList lastCave
                |> List.filter (caveIsEligibleForVisit path)
    in
    case ( lastCave, eligibleNeighbors ) of
        ( "end", _ ) ->
            [ path ]

        ( _, [] ) ->
            []

        _ ->
            eligibleNeighbors
                |> List.map (appendToPath path)
                |> List.map (completePath adjacencyList)
                |> List.concat


completePathPartTwo : AdjacencyList -> Path -> List Path
completePathPartTwo adjacencyList path =
    let
        lastCave =
            endingCave path

        eligibleNeighbors =
            neighborsOf adjacencyList lastCave
                |> List.filter (caveIsEligibleForVisitAtMostOneSmallCaveTwice path)
    in
    case ( lastCave, eligibleNeighbors ) of
        ( "end", _ ) ->
            [ path ]

        ( _, [] ) ->
            []

        _ ->
            eligibleNeighbors
                |> List.map (appendToPath path)
                |> List.map (completePathPartTwo adjacencyList)
                |> List.concat


stringsToAdjacencyList : List String -> AdjacencyList
stringsToAdjacencyList entries =
    let
        entryToCaves : String -> ( String, String )
        entryToCaves entry =
            let
                separatorIndex =
                    String.indices "-" entry
                        |> List.head
                        |> Maybe.withDefault -1

                leftCave =
                    String.left separatorIndex entry

                rightCave =
                    String.dropLeft (separatorIndex + 1) entry
            in
            ( leftCave, rightCave )

        recordAdjacency : String -> String -> AdjacencyList -> AdjacencyList
        recordAdjacency from to adjacencyList =
            let
                value =
                    Dict.get from adjacencyList
                        |> Maybe.withDefault []
            in
            Dict.insert from (to :: value) adjacencyList

        addCavesToAdjacencyList : ( String, String ) -> AdjacencyList -> AdjacencyList
        addCavesToAdjacencyList ( a, b ) adjacencyList =
            adjacencyList
                |> recordAdjacency a b
                |> recordAdjacency b a
    in
    List.foldl (entryToCaves >> addCavesToAdjacencyList) Dict.empty entries



------------------ part 2


input =
    [ "pg-CH"
    , "pg-yd"
    , "yd-start"
    , "fe-hv"
    , "bi-CH"
    , "CH-yd"
    , "end-bi"
    , "fe-RY"
    , "ng-CH"
    , "fe-CH"
    , "ng-pg"
    , "hv-FL"
    , "FL-fe"
    , "hv-pg"
    , "bi-hv"
    , "CH-end"
    , "hv-ng"
    , "yd-ng"
    , "pg-fe"
    , "start-ng"
    , "end-FL"
    , "fe-bi"
    , "FL-ks"
    , "pg-start"
    ]
