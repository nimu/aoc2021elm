module Day08Tests exposing (suite)

import Day08 exposing (Pattern(..), Seg(..))
import Dict
import Expect exposing (Expectation)
import Test exposing (..)


suite : Test
suite =
    describe "Day08 module"
        [ describe "prepare Pattern"
            [ test "results in pattern of same length" <|
                \_ ->
                    let
                        input =
                            "abcde"
                    in
                    Expect.equal (String.length input) (Day08.patternLength (Day08.preparePattern input))
            ]
        , describe "prepare Output"
            [ test "results in output of same length" <|
                \_ ->
                    let
                        input =
                            "abcde gca abe"
                    in
                    Expect.equal (List.length <| String.split " " input) (Day08.outputLength (Day08.prepareOutput input))
            ]
        , describe "patternOfUniqueLength"
            [ test "Correctly recognizes uniqueLength Patterns" <|
                \_ ->
                    let
                        inputs =
                            [ "abcd", "gca", "abe", "ab", "abcdefg", "geaf" ]
                    in
                    inputs
                        |> List.map Day08.preparePattern
                        |> List.all Day08.patternOfUniqueLength
                        |> Expect.true "all of them have unique length"
            ]
        , describe "countUniquePatternsInDisplays"
            [ test "Correctly counts uniqueLength Patterns in display" <|
                \_ ->
                    let
                        inputs =
                            [ "eacd gdc acfdeg begdfac cfdga agefd bcgfa bgefdc fegadb dc | cd cd dabegcf ecfgabd"
                            ]
                    in
                    Day08.countUniquePatternsInDisplays inputs
                        |> Expect.equal 4
            ]
        , describe "rewirePattern"
            [ test "Correctly rewires Pattern" <|
                \_ ->
                    let
                        rewiring =
                            Dict.fromList
                                [ ( 'a', 'g' )
                                , ( 'b', 'f' )
                                , ( 'c', 'e' )
                                , ( 'd', 'd' )
                                , ( 'e', 'c' )
                                , ( 'f', 'b' )
                                , ( 'g', 'a' )
                                ]

                        input =
                            Day08.preparePattern
                                "abdefg"
                    in
                    Day08.rewirePattern rewiring input
                        |> Expect.equal (Pattern [ A, B, C, D, F, G ])
            ]
        , describe "displayToValue"
            [ test "Correctly interprets display output" <|
                \_ ->
                    let
                        input =
                            "eacd gdc acfdeg begdfac cfdga agefd bcgfa bgefdc fegadb dc | cd cd gdc ecad"
                    in
                    Day08.prepareInput input
                        |> Day08.displayToValue
                        |> Expect.equal 1174
            ]
        ]
