module Day10Tests exposing (suite)

import Day10 exposing (..)
import Expect exposing (Expectation)
import Test exposing (..)


suite : Test
suite =
    describe "Day10 module"
        [ describe "findFirstIllegalCharacter"
            [ test "finds first illegal brace" <|
                \_ ->
                    let
                        input =
                            "{([(<{}[<>[]}>{[]{[(<()>"
                    in
                    Expect.equal 1197 (Day10.findFirstIllegalCharacter input |> violationToPrize)
            , test "finds first illegal paren" <|
                \_ ->
                    let
                        input =
                            "[[<[([]))<([[{}[[()]]]"
                    in
                    Expect.equal 3 (Day10.findFirstIllegalCharacter input |> violationToPrize)
            ]
        , describe "mapToStackIfIncomplete and stackToPrize"
            [ test "finds and evaluates stack correctly" <|
                \_ ->
                    let
                        input =
                            "[({(<(())[]>[[{[]{<()<>>"
                    in
                    Expect.equal 288957 <| (Day10.mapToStackIfIncomplete >> Day10.stackToPrize) input
            ]
        ]
