module Day20Tests exposing (suite)

import Day20 exposing (kernelValuesToNumber)
import Expect exposing (Expectation)
import Test exposing (..)


suite : Test
suite =
    describe "Day20 module"
        [ describe "kernel to number"
            [ test "example kernel interpreted correctly" <|
                \_ ->
                    let
                        input =
                            "...#...#."
                    in
                    Expect.equal 34 (kernelValuesToNumber input)
            , test "kernel centeredAt" <|
                \_ ->
                    let
                        input =
                            ( 0, 0 )
                    in
                    Expect.equal
                        [ ( -1, -1 )
                        , ( 0, -1 )
                        , ( 1, -1 )
                        , ( -1, 0 )
                        , ( 0, 0 )
                        , ( 1, 0 )
                        , ( -1, 1 )
                        , ( 0, 1 )
                        , ( 1, 1 )
                        ]
                        (Day20.kernelCenteredAt input)
            ]
        ]
