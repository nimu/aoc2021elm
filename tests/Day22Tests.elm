module Day22Tests exposing (suite)

import Day22 exposing (..)
import Expect exposing (Expectation)
import Parser
import Test exposing (..)


suite : Test
suite =
    describe "Day22 module"
        [ describe "parse reboots"
            [ test "parses reboots beginning with negative numbers" <|
                \_ ->
                    let
                        input =
                            "on x= -12 .. 34,y= -23 .. 45,z= -34 .. 56"
                    in
                    Expect.equal (Parser.run rebootsParser input) <| Ok <| On (Cuboid (Range -12 34) (Range -23 45) (Range -34 56))
            , test "parses reboots beginning with positive numbers" <|
                \_ ->
                    let
                        input =
                            "on x= 12 .. 34,y= 23 .. 45,z= 34 .. 56"
                    in
                    Expect.equal (Parser.run rebootsParser input) <| Ok <| On (Cuboid (Range 12 34) (Range 23 45) (Range 34 56))
            , test "parses off reboots beginning with any numbers" <|
                \_ ->
                    let
                        input =
                            "off x= 12 .. 34,y= 23 .. 45,z= 34 .. 56"
                    in
                    Expect.equal (Parser.run rebootsParser input) <| Ok <| Off (Cuboid (Range 12 34) (Range 23 45) (Range 34 56))
            ]
        , test "deconstructs Range without duplicates" <|
            \_ ->
                let
                    input =
                        [ 1, 2, 3 ]
                in
                Expect.equal (listToRanges Nothing input) [ Range 1 2, Range 2 3 ]
        , test "deconstructs Range with duplicates" <|
            \_ ->
                let
                    input =
                        [ 1, 2, 2, 3, 3 ]
                in
                Expect.equal (listToRanges Nothing input) [ Range 1 2, Range 2 3 ]
        , test "deconstructs Range with only two values" <|
            \_ ->
                let
                    input =
                        [ 1, 2 ]
                in
                Expect.equal (listToRanges Nothing input) [ Range 1 2 ]
        , test "Range size" <|
            \_ ->
                let
                    input =
                        Range 1 2
                in
                Expect.equal (rangeSize input) 2
        , test "Cuboid size" <|
            \_ ->
                let
                    input =
                        Cuboid (Range 1 1) (Range 11 20) (Range 101 200)
                in
                Expect.equal (cuboidSize input) 1000
        , test "Cuboid size from input" <|
            \_ ->
                let
                    input =
                        Cuboid (Range -20 29) (Range -31 16) (Range -20 27)
                in
                Expect.equal (cuboidSize input) 115200
        , test "Solve part 2 small sample" <|
            \_ ->
                let
                    input =
                        Day22.solvePart2For Day22.inputInBounds

                    resultWithPart1 =
                        Day22.solvePart1For Day22.inputInBounds
                in
                Expect.equal input resultWithPart1
        , test "Solve part 2 on part 2 sample" <|
            \_ ->
                let
                    input =
                        Day22.solvePart2For Day22.samplePartTwo
                in
                Expect.equal 2758514936282235 input
        , test "Remove intersections" <|
            \_ ->
                let
                    original =
                        Cuboid (Range 1 10) (Range 1 10) (Range 1 10)

                    intersection =
                        Cuboid (Range 1 1) (Range 1 1) (Range 1 1)

                    intersectionRemoved =
                        Debug.log "Intersection removed" <|
                            Day22.removeIntersections intersection original

                    sizeAfterRemovingIntersections =
                        intersectionRemoved
                            |> List.map cuboidSize
                            |> List.sum
                in
                Expect.equal 999 sizeAfterRemovingIntersections
        ]
