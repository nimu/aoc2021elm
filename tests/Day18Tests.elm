module Day18Tests exposing (suite)

import Day18 exposing (..)
import Expect exposing (Expectation)
import List.Extra
import Parser
import Test exposing (..)


suite : Test
suite =
    describe "Day18 module"
        [ describe "parser"
            [ test "parses leaf" <|
                \_ ->
                    let
                        input =
                            "123"
                    in
                    Expect.equal (Result.Ok <| Leaf 123) (Parser.run Day18.parser input)
            , test "parses simple node" <|
                \_ ->
                    let
                        input =
                            "[1,2]"
                    in
                    Expect.equal (Result.Ok <| Node (Leaf 1) (Leaf 2)) (Parser.run Day18.parser input)
            , test "parses nested node in first position" <|
                \_ ->
                    let
                        input =
                            "[[1,2],3]"
                    in
                    Expect.equal (Result.Ok <| Node (Node (Leaf 1) (Leaf 2)) (Leaf 3)) (Parser.run Day18.parser input)
            , test "parses nested node in second position" <|
                \_ ->
                    let
                        input =
                            "[1,[2,3]]"
                    in
                    Expect.equal (Result.Ok <| Node (Leaf 1) (Node (Leaf 2) (Leaf 3))) (Parser.run Day18.parser input)
            , test "split Leaf" <|
                \_ ->
                    let
                        input =
                            Leaf 10
                    in
                    Expect.equal (splitIfNeeded input) (Node (Leaf 5) (Leaf 5))
            , test "split only one Leaf" <|
                \_ ->
                    let
                        input =
                            Node (Leaf 10) (Leaf 10)
                    in
                    Expect.equal (splitIfNeeded input) (Node (Node (Leaf 5) (Leaf 5)) (Leaf 10))
            , test "find explosion candidate finds right additions" <|
                \_ ->
                    let
                        input =
                            "[[6,[5,[4,[3,2]]]],1]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionCandidate =
                            findExplosionCandidateFor input
                    in
                    Expect.equal (Maybe.map .additionLeft explosionCandidate) (Just 3)
            , test "find explosion candidate finds nothing" <|
                \_ ->
                    let
                        input =
                            "[[6,[5,[4,3]]],1]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionCandidate =
                            findExplosionCandidateFor input
                    in
                    Expect.equal explosionCandidate Nothing
            , test "explode example 1" <|
                \_ ->
                    let
                        input =
                            "[[[[[9,8],1],2],3],4]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        output =
                            "[[[[0,9],2],3],4]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionDone =
                            explodeIfNeeded input
                    in
                    Expect.equal explosionDone output
            , test "explode example 2" <|
                \_ ->
                    let
                        input =
                            "[7,[6,[5,[4,[3,2]]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        output =
                            "[7,[6,[5,[7,0]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionDone =
                            explodeIfNeeded input
                    in
                    Expect.equal explosionDone output
            , test "explode example 3" <|
                \_ ->
                    let
                        input =
                            "[[6,[5,[4,[3,2]]]],1]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        output =
                            "[[6,[5,[7,0]]],3]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionDone =
                            explodeIfNeeded input
                    in
                    Expect.equal explosionDone output
            , test "explode example 4" <|
                \_ ->
                    let
                        input =
                            "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        output =
                            "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionDone =
                            explodeIfNeeded input
                    in
                    Expect.equal explosionDone output
            , test "explode example 5" <|
                \_ ->
                    let
                        input =
                            "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        output =
                            "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        explosionDone =
                            explodeIfNeeded input
                    in
                    Expect.equal explosionDone output
            , test "reduction example" <|
                \_ ->
                    let
                        input =
                            "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        output =
                            "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        reduced =
                            reduce input
                    in
                    Expect.equal reduced output
            , test "magnitude example" <|
                \_ ->
                    let
                        input =
                            "[[[[6,6],[7,6]],[[7,7],[7,0]]],[[[7,7],[7,7]],[[7,8],[9,9]]]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber
                    in
                    Expect.equal (magnitude input) 4140
            , test "Part 1 sample solution" <|
                \_ ->
                    let
                        part1SampleSolution =
                            solvePart1For sample
                    in
                    Expect.equal part1SampleSolution 4140
            , test "Example addition 1" <|
                \_ ->
                    let
                        input =
                            [ "[1,1]"
                            , "[2,2]"
                            , "[3,3]"
                            , "[4,4]"
                            ]
                                |> List.map (Parser.run parser >> Result.withDefault InvalidNumber)

                        output =
                            "[[[[1,1],[2,2]],[3,3]],[4,4]]"
                                |> Parser.run Day18.parser
                                |> Result.withDefault InvalidNumber

                        sum =
                            List.Extra.foldl1 (\b -> \a -> add a b |> reduce) input
                                |> Maybe.withDefault InvalidNumber
                    in
                    Expect.equal (toString sum) (toString output)
            ]
        ]
