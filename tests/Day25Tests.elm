module Day25Tests exposing (suite)

import Day18 exposing (..)
import Day25 exposing (Cucumber(..), seafloorToString)
import Expect exposing (Expectation)
import List.Extra
import Matrix
import Parser
import Test exposing (..)


suite : Test
suite =
    describe "Day25 module"
        [ describe "parser"
            [ test "parses southfacing" <|
                \_ ->
                    let
                        input =
                            "v"
                    in
                    Expect.equal (Result.Ok (Just Southfacing)) (Parser.run Day25.cellParser input)
            , test "parses eastfacing" <|
                \_ ->
                    let
                        input =
                            ">"
                    in
                    Expect.equal (Result.Ok (Just Eastfacing)) (Parser.run Day25.cellParser input)
            , test "parses empty" <|
                \_ ->
                    let
                        input =
                            "."
                    in
                    Expect.equal (Result.Ok Nothing) (Parser.run Day25.cellParser input)
            , test "parses row" <|
                \_ ->
                    let
                        input =
                            ".v>.>"
                    in
                    Expect.equal (Result.Ok [ Nothing, Just Southfacing, Just Eastfacing, Nothing, Just Eastfacing ]) (Parser.run Day25.rowParser input)
            , test "moves east when no obstructions" <|
                \_ ->
                    let
                        input =
                            ".v>.>"
                                |> Parser.run Day25.rowParser
                                |> Result.withDefault [ Nothing ]
                                |> List.singleton
                                |> Matrix.fromList

                        output =
                            Day25.move Eastfacing input
                    in
                    Expect.equal ">v.>." (seafloorToString output)
            ]
        ]
